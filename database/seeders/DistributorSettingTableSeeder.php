<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DistributorSettingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('DISTRIBUTOR_SETTING')->delete();

        \DB::table('DISTRIBUTOR_SETTING')->insert(array(
            0 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => '',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'DIRECT',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'MARKETING_APPROACH',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 1,
            ),
            1 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => '',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'SINGLE-TIER',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'TYPE_STRUCTURE',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 3,
            ),
            2 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'WILL BE REMOVE',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'ACTIVE',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'STATUS',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 5,
            ),
            3 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'WILL BE REMOVE',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'SUSPEND',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'STATUS',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 6,
            ),
            4 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'REGISTERED/RENEWED',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => '',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'DECLARATION',
                'DIST_SET_VALUE' => '5',
                'DISTRIBUTOR_SETTING_ID' => 10,
            ),
            5 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'WILL BE REMOVE',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'BARRED',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'STATUS',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 11,
            ),
            6 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'WILL BE REMOVE',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'BLACKLIST',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'STATUS',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 12,
            ),
            7 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'SYSTEM WILL BLOCK CONSULTANT TO FURTHER REGISTER TO BE A CONSULTANT UNTIL BARRED PERIOD ENDS.',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => '',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'DECLARATION',
                'DIST_SET_VALUE' => '11',
                'DISTRIBUTOR_SETTING_ID' => 13,
            ),
            8 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => '',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'NOMINEE',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'MARKETING_APPROACH',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 28,
            ),
            9 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => '',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'MULTI-TIER',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'TYPE_STRUCTURE',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 29,
            ),
            10 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'WILL BE REMOVE',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => 'REVOKE',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'STATUS',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 30,
            ),
            11 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => '',
                'DIST_SET_DESCRIPTION' => 'WITHDRAW OF REGISTRATION',
                'DIST_SET_INDEX' => 0,
                'DIST_SET_PARAM' => '',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'CESSATION',
                'DIST_SET_VALUE' => '',
                'DISTRIBUTOR_SETTING_ID' => 31,
            ),
            12 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => NULL,
                'DIST_SET_DESCRIPTION' => 'MERGER AND ACQUISITION',
                'DIST_SET_INDEX' => NULL,
                'DIST_SET_PARAM' => NULL,
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'CESSATION',
                'DIST_SET_VALUE' => NULL,
                'DISTRIBUTOR_SETTING_ID' => 34,
            ),
            13 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => NULL,
                'DIST_SET_DESCRIPTION' => NULL,
                'DIST_SET_INDEX' => NULL,
                'DIST_SET_PARAM' => 'FUND',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'DIVESTMENT',
                'DIST_SET_VALUE' => NULL,
                'DISTRIBUTOR_SETTING_ID' => 35,
            ),
            14 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => NULL,
                'DIST_SET_DESCRIPTION' => NULL,
                'DIST_SET_INDEX' => NULL,
                'DIST_SET_PARAM' => 'CONSULTANT',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'DIVESTMENT',
                'DIST_SET_VALUE' => NULL,
                'DISTRIBUTOR_SETTING_ID' => 36,
            ),
            15 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => NULL,
                'DIST_SET_DESCRIPTION' => NULL,
                'DIST_SET_INDEX' => NULL,
                'DIST_SET_PARAM' => 'FUND AND CONSULTANT',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'DIVESTMENT',
                'DIST_SET_VALUE' => NULL,
                'DISTRIBUTOR_SETTING_ID' => 37,
            ),
            16 =>
            array(
                'DIST_CREATE_BY' => NULL,
                'DIST_CREATE_TIMESTAMP' => NULL,
                'DIST_SET_CODE' => NULL,
                'DIST_SET_DESCRIPTION' => NULL,
                'DIST_SET_INDEX' => NULL,
                'DIST_SET_PARAM' => 'AUTO ACCEPTANCE DAYS',
                'DIST_SET_REMARK' => NULL,
                'DIST_SET_TYPE' => 'SUSPEND_REVOKE',
                'DIST_SET_VALUE' => '14',
                'DISTRIBUTOR_SETTING_ID' => 38,
            ),
        ));
    }
}
