<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TaskStatusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('TASK_STATUS')->delete();

        \DB::table('TASK_STATUS')->insert(array(
            0 =>
            array(
                'TS_CODE' => 'GENERAL',
                'TS_DESCRIPTION' => 'IF STATUS = 0 ',
                'TS_ID' => 1,
                'TS_INDEX' => 1,
                'TS_PARAM' => 'DRAFT',
                'TS_REMARK' => NULL,
            ),
            1 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => 'IF STATUS =1',
                'TS_ID' => 2,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'NEW ENTRY',
                'TS_REMARK' => NULL,
            ),
            2 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 3,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'APPROVED',
                'TS_REMARK' => 'GENERAL',
            ),
            3 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 4,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'VERIFIED',
                'TS_REMARK' => 'GENERAL',
            ),
            4 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 5,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'REJECTED',
                'TS_REMARK' => 'GENERAL',
            ),
            5 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 6,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'ACKNOWLEDGED',
                'TS_REMARK' => '',
            ),
            6 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 7,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RETURNED',
                'TS_REMARK' => 'RETURNED TO DISTRIBUTOR',
            ),
            7 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 8,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RETURNED',
                'TS_REMARK' => 'GENERAL',
            ),
            8 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => 'CREATOR : E.G.  RD ADMIN, LRA ADMIN',
                'TS_ID' => 9,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RETURNED',
                'TS_REMARK' => 'RETURNED TO CREATOR',
            ),
            9 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 14,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'REVIEWED',
                'TS_REMARK' => 'GENERAL',
            ),
            10 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 15,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'PENDING',
                'TS_REMARK' => 'GENERAL',
            ),
            11 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 16,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'APPEAL',
                'TS_REMARK' => NULL,
            ),
            12 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 17,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'ACCEPTED',
                'TS_REMARK' => 'GENERAL',
            ),
            13 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 18,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RECOMMENDED',
                'TS_REMARK' => NULL,
            ),
            14 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => 'REQUEST FOR RETURN',
                'TS_ID' => 19,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'REQUEST RETURN',
                'TS_REMARK' => NULL,
            ),
            15 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 20,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'PAID',
                'TS_REMARK' => NULL,
            ),
            16 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 21,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'UPDATED',
                'TS_REMARK' => NULL,
            ),
            17 =>
            array(
                'TS_CODE' => 'GENERAL',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 22,
                'TS_INDEX' => 2,
                'TS_PARAM' => 'ACTIVE',
                'TS_REMARK' => 'DOT CHANGE THE TS_CODE = GENERAL, use for general status only. dont add other param accept for draft,cancel, active & inactive',
            ),
            18 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 23,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'SUSPENDED',
                'TS_REMARK' => NULL,
            ),
            19 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 24,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'DELETED',
                'TS_REMARK' => NULL,
            ),
            20 =>
            array(
                'TS_CODE' => 'GENERAL',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 25,
                'TS_INDEX' => 3,
                'TS_PARAM' => 'INACTIVE',
                'TS_REMARK' => NULL,
            ),
            21 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 26,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'ACTIVE',
                'TS_REMARK' => 'PENDING DELETION APPROVAL',
            ),
            22 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 27,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RETURNED',
                'TS_REMARK' => 'RETURNED TO ID',
            ),
            23 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 28,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RETURNED',
                'TS_REMARK' => 'RETURNED BY RD',
            ),
            24 =>
            array(
                'TS_CODE' => 'GENERAL',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 29,
                'TS_INDEX' => 4,
                'TS_PARAM' => 'CANCEL',
                'TS_REMARK' => NULL,
            ),
            25 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 30,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'REVOKED',
                'TS_REMARK' => NULL,
            ),
            26 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => 'FUND MANAGER APPROVED',
                'TS_ID' => 31,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'APPROVED',
                'TS_REMARK' => NULL,
            ),
            27 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 32,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'UNPAID',
                'TS_REMARK' => NULL,
            ),
            28 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => 'WAITING EXAMINATION RESULT',
                'TS_ID' => 33,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'EXAMINATION',
                'TS_REMARK' => NULL,
            ),
            29 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 34,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'PASS',
                'TS_REMARK' => NULL,
            ),
            30 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 35,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'FAILED',
                'TS_REMARK' => NULL,
            ),
            31 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 36,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RE-SCHEDULE',
                'TS_REMARK' => NULL,
            ),
            32 =>
            array(
                'TS_CODE' => NULL,
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 37,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'RESUBMIT',
                'TS_REMARK' => NULL,
            ),
            33 =>
            array(
                'TS_CODE' => 'GENERAL-CONSULTANT',
                'TS_DESCRIPTION' => NULL,
                'TS_ID' => 38,
                'TS_INDEX' => 0,
                'TS_PARAM' => 'COMPLETED',
                'TS_REMARK' => NULL,
            ),
        ));
    }
}
