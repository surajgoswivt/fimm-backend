<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DistributorTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('DISTRIBUTOR_TYPE')->delete();

        \DB::table('DISTRIBUTOR_TYPE')->insert(array(
            0 =>
            array(
                'ANNUALFEES_ID' => 1,
                'DIST_TYPE_NAME' => 'UTMC',
                'DIST_TYPE_VARIATION' => 'PRP | IUTA | IPRA',
                'DISTRIBUTOR_TYPE_ID' => 1,
                'MARKETING_APPROACH_ID' => 1,
                'SCHEME' => '1',
                'TYPE_STRUCTURE_ID' => 3,
            ),
            1 =>
            array(
                'ANNUALFEES_ID' => 4,
                'DIST_TYPE_NAME' => 'PRP',
                'DIST_TYPE_VARIATION' => 'UTMC | IUTA | IPRA',
                'DISTRIBUTOR_TYPE_ID' => 2,
                'MARKETING_APPROACH_ID' => 1,
                'SCHEME' => '2',
                'TYPE_STRUCTURE_ID' => 3,
            ),
            2 =>
            array(
                'ANNUALFEES_ID' => 2,
                'DIST_TYPE_NAME' => 'IUTA',
                'DIST_TYPE_VARIATION' => 'UTMC | PRP | IPRA',
                'DISTRIBUTOR_TYPE_ID' => 3,
                'MARKETING_APPROACH_ID' => 1,
                'SCHEME' => '1',
                'TYPE_STRUCTURE_ID' => 3,
            ),
            3 =>
            array(
                'ANNUALFEES_ID' => 3,
                'DIST_TYPE_NAME' => 'CUTA',
                'DIST_TYPE_VARIATION' => 'CPRA',
                'DISTRIBUTOR_TYPE_ID' => 4,
                'MARKETING_APPROACH_ID' => 2,
                'SCHEME' => '1',
                'TYPE_STRUCTURE_ID' => 4,
            ),
            4 =>
            array(
                'ANNUALFEES_ID' => 6,
                'DIST_TYPE_NAME' => 'CPRA',
                'DIST_TYPE_VARIATION' => 'CUTA',
                'DISTRIBUTOR_TYPE_ID' => 5,
                'MARKETING_APPROACH_ID' => 2,
                'SCHEME' => '2',
                'TYPE_STRUCTURE_ID' => 4,
            ),
            5 =>
            array(
                'ANNUALFEES_ID' => 5,
                'DIST_TYPE_NAME' => 'IPRA',
                'DIST_TYPE_VARIATION' => 'UTMC | PRP | IUTA',
                'DISTRIBUTOR_TYPE_ID' => 6,
                'MARKETING_APPROACH_ID' => NULL,
                'SCHEME' => '2',
                'TYPE_STRUCTURE_ID' => NULL,
            ),
        ));
    }
}
