<?php

namespace Database\Seeders;
use App\Models\Modules;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = new Modules;
        $module->code = 'UH10000';
        $module->name = 'Admin Module';
        $module->short_name = 'Admin';
        $module->icon = '';
        $module->index = '1';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10001';
        $module->name = 'Distributor Management Module';
        $module->short_name = 'Distributor';
        $module->icon = '';
        $module->index = '2';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10002';
        $module->name = 'Consultant Management Module';
        $module->short_name = 'Consultant';
        $module->icon = '';
        $module->index = '3';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10003';
        $module->name = 'Consultant Alert Module';
        $module->short_name = 'Consultant Alert';
        $module->icon = '';
        $module->index = '4';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10004';
        $module->name = 'CPD Module';
        $module->short_name = 'CPD';
        $module->icon = '';
        $module->index = '5';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10005';
        $module->name = 'Fund Management Module';
        $module->short_name = 'Fund';
        $module->icon = '';
        $module->index = '6';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10006';
        $module->name = 'Finance Module';
        $module->short_name = 'Finance';
        $module->icon = '';
        $module->index = '7';
        $module->save();

        $module = new Modules;
        $module->code = 'UH10007';
        $module->name = 'Annual Fee Module';
        $module->short_name = 'Annual Fee';
        $module->icon = '';
        $module->index = '8';
        $module->save();
    }
}
