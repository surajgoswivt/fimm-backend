<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call([
        //     TestSeeder::class,
        //     SettingSeeder::class,
        //     ModuleSeeder::class,
        //     EmailSeeder::class
        // ]);

        $this->call(ApprovalLevelTableSeeder::class);
        $this->call(ConsultantTypeTableSeeder::class);
        $this->call(DistributorSettingTableSeeder::class);
        $this->call(DistributorTypeTableSeeder::class);
        $this->call(ProcessFlowTableSeeder::class);
        $this->call(SettingGeneralTableSeeder::class);
        $this->call(TaskStatusTableSeeder::class);
    }
}
