<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProcessFlowTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('PROCESS_FLOW')->delete();

        \DB::table('PROCESS_FLOW')->insert(array(
            0 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 1,
                'PROCESS_FLOW_NAME' => 'NEW DISTIBUTOR - A(1)(a)',
            ),
            1 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 2,
                'PROCESS_FLOW_NAME' => 'UPDATE DISTRIBUTOR DETAILS - A(1)(b)',
            ),
            2 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 3,
                'PROCESS_FLOW_NAME' => 'CESSATION OF OPERATION - A(1)(c)(i)',
            ),
            3 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 4,
                'PROCESS_FLOW_NAME' => 'SUSPENSION AND REVOCATION - A(1)(c)(ii)',
            ),
            4 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 5,
                'PROCESS_FLOW_NAME' => 'DIVESTMENT - A(1)(d)',
            ),
            5 =>
            array(
                'MODULE_ID' => '4',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 6,
                'PROCESS_FLOW_NAME' => 'CONSULTANT CASE MANAGEMENT - B(1)(a)',
            ),
            6 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 8,
                'PROCESS_FLOW_NAME' => 'NEW CONSULTANT - A(2)(a)',
            ),
            7 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 9,
                'PROCESS_FLOW_NAME' => 'TERMINATION - A(2)(d)',
            ),
            8 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 10,
                'PROCESS_FLOW_NAME' => 'BANKRUPTCY CHECK - A(2)(i)
',
            ),
            9 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 11,
                'PROCESS_FLOW_NAME' => 'APPEAL- A(2)(j)
',
            ),
            10 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 12,
                'PROCESS_FLOW_NAME' => 'POST-VETTING - D(1)(a)',
            ),
            11 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 13,
                'PROCESS_FLOW_NAME' => 'PRE-VETTING - D(1)(b)',
            ),
            12 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 14,
                'PROCESS_FLOW_NAME' => 'FIVE MODULES SUBMISSION - D(1)(c)',
            ),
            13 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 15,
                'PROCESS_FLOW_NAME' => 'WAIVER MODULE SUBMISSION- D(1)(d)',
            ),
            14 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 16,
                'PROCESS_FLOW_NAME' => 'REVOCATION - D(1)(f)',
            ),
            15 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 17,
                'PROCESS_FLOW_NAME' => 'NEW TRAINING PROVIDER - D(1)(g)',
            ),
            16 =>
            array(
                'MODULE_ID' => '7',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 18,
                'PROCESS_FLOW_NAME' => 'COMPANY REGISTRATION PAYMENT - E(1)(a)',
            ),
            17 =>
            array(
                'MODULE_ID' => '7',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 19,
                'PROCESS_FLOW_NAME' => 'PREPAYMENT TOPUP - E(1)(b)',
            ),
            18 =>
            array(
                'MODULE_ID' => '7',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 20,
                'PROCESS_FLOW_NAME' => 'CONSULTANT REGISTRATION PAYMENT - E(1)(c)',
            ),
            19 =>
            array(
                'MODULE_ID' => '7',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 21,
                'PROCESS_FLOW_NAME' => 'PREPAYMENT REFUND - E(1)(d)
',
            ),
            20 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 22,
                'PROCESS_FLOW_NAME' => 'FUND CREATION - F(1)(a)(f)(i)',
            ),
            21 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 23,
                'PROCESS_FLOW_NAME' => 'FUND LOGDEMENT - F(1)(a)(f)(ii)',
            ),
            22 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 24,
                'PROCESS_FLOW_NAME' => 'FUND TERMINATION - F(1)(b)',
            ),
            23 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 25,
                'PROCESS_FLOW_NAME' => 'FUND MANAGEMENT - F(1)(g)',
            ),
            24 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 26,
                'PROCESS_FLOW_NAME' => 'NAV MANAGEMENT - F(1)(e)',
            ),
            25 =>
            array(
                'MODULE_ID' => '6',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 27,
                'PROCESS_FLOW_NAME' => 'INTEGRATE WITH ANNUAL FEE - G(1)(a)
',
            ),
            26 =>
            array(
                'MODULE_ID' => '10',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 28,
                'PROCESS_FLOW_NAME' => 'CIRCULAR MANAGEMENT - L(1)(g)',
            ),
            27 =>
            array(
                'MODULE_ID' => '10',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 29,
                'PROCESS_FLOW_NAME' => 'ANNOUCEMENT MANAGEMENT - L(1)(j)',
            ),
            28 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 34,
                'PROCESS_FLOW_NAME' => 'EXTENSION TIME A(1)(i)',
            ),
            29 =>
            array(
                'MODULE_ID' => '',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 35,
                'PROCESS_FLOW_NAME' => 'BULK UPLOAD REGISTRATION A(2)(k)',
            ),
            30 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 36,
                'PROCESS_FLOW_NAME' => 'RE-REGISTER A(2)(b)',
            ),
            31 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 37,
                'PROCESS_FLOW_NAME' => 'EXEMPTION A(2)(c)',
            ),
            32 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 38,
                'PROCESS_FLOW_NAME' => 'RESIGNATION A(2)(e)',
            ),
            33 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 39,
                'PROCESS_FLOW_NAME' => 'RENEWAL A(2)(f)',
            ),
            34 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 40,
                'PROCESS_FLOW_NAME' => 'UPDATE PROFILE A(2)(g)',
            ),
            35 =>
            array(
                'MODULE_ID' => '3',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 41,
                'PROCESS_FLOW_NAME' => 'RE_SCHEDULE A(2)(h)',
            ),
            36 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 42,
                'PROCESS_FLOW_NAME' => 'REPEATED POST-VETTING SUBMISSION D(1)(a)(i)',
            ),
            37 =>
            array(
                'MODULE_ID' => '',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 43,
                'PROCESS_FLOW_NAME' => 'REPEATED PRE-VETTING SUBMISSION D(1)(b)(ii)',
            ),
            38 =>
            array(
                'MODULE_ID' => '9',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 44,
                'PROCESS_FLOW_NAME' => 'AMENDMENT D(1)(e)',
            ),
            39 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 45,
                'PROCESS_FLOW_NAME' => 'NAV DAILY PUBLISH F(1)(c)',
            ),
            40 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 46,
                'PROCESS_FLOW_NAME' => 'NAV DAILY UPDATE F(1)(d)',
            ),
            41 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 48,
                'PROCESS_FLOW_NAME' => 'THIRD PARTY COMPANY REGISTRATION D(1)(h)',
            ),
            42 =>
            array(
                'MODULE_ID' => '5',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 49,
                'PROCESS_FLOW_NAME' => 'FUND DISLODGEMENT F(1)(i)',
            ),
            43 =>
            array(
                'MODULE_ID' => '8',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 50,
                'PROCESS_FLOW_NAME' => 'PREPAYMENT FOR EXAM WAIVER E(1)(d)(ii)',
            ),
            44 =>
            array(
                'MODULE_ID' => '8',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 51,
                'PROCESS_FLOW_NAME' => 'AMSF COLLECTION E(1)(f)',
            ),
            45 =>
            array(
                'MODULE_ID' => '10',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 52,
                'PROCESS_FLOW_NAME' => 'NEW SYSTEM USER H(1)(a)',
            ),
            46 =>
            array(
                'MODULE_ID' => '7',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 56,
                'PROCESS_FLOW_NAME' => 'PREPAYMENT REFUND EXAM WAIVER',
            ),
            47 =>
            array(
                'MODULE_ID' => '7',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 57,
                'PROCESS_FLOW_NAME' => 'AMSF COLLECTION',
            ),
            48 =>
            array(
                'MODULE_ID' => '2',
                'PF_INDEX' => NULL,
                'PROCESS_FLOW_ID' => 58,
                'PROCESS_FLOW_NAME' => 'EXTENSION TIME - A(1)(i)',
            ),
        ));
    }
}
