<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting;
        $setting->KEYCLOAK_BASE_URL = '';
        $setting->KEYCLOAK_TOKEN_URL = 'http://v.irc.my:8057/auth/realms/ldap-realm/protocol/openid-connect/token';
        $setting->KEYCLOAK_LOGOUT_URL = 'http://v.irc.my:8057/auth/realms/ldap-realm/protocol/openid-connect/logout';
        $setting->KEYCLOAK_REALM_NAME = 'ldap-realm';
        $setting->KEYCLOAK_REALM_PUBLIC_KEY = '';
        $setting->KEYCLOAK_CLIENT_ID = 'reg-client';
        $setting->KEYCLOAK_CLIENT_SECRET = 'd8192ded-9f0e-4ab0-b716-50eaf0da60c4';
        $setting->save();

        $setting = new Setting;
        $setting->KEYCLOAK_BASE_URL = '';
        $setting->KEYCLOAK_TOKEN_URL = 'http://v.irc.my:8057/auth/realms/ldap-realm/protocol/openid-connect/token';
        $setting->KEYCLOAK_LOGOUT_URL = 'http://v.irc.my:8057/auth/realms/ldap-realm/protocol/openid-connect/logout';
        $setting->KEYCLOAK_REALM_NAME = 'ldap-realm';
        $setting->KEYCLOAK_REALM_PUBLIC_KEY = '';
        $setting->KEYCLOAK_CLIENT_ID = 'eplatform-client';
        $setting->KEYCLOAK_CLIENT_SECRET = '83accdfc-eb3e-40a8-af24-dce93fb7d147';
        $setting->save();
    }
}
