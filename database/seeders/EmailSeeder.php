<?php

namespace Database\Seeders;
use App\Models\Email;
use Illuminate\Database\Seeder;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = new Email;
        $email->email_protocol = 'smtp';
        $email->email_host = 'smtp.gmail.com';
        $email->email_port = 587;
        $email->email_username = 'fi@gmail.com';
        $email->email_password = '';
        $email->save();
    }
}
