<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ConsultantTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('CONSULTANT_TYPE')->delete();

        \DB::table('CONSULTANT_TYPE')->insert(array(
            0 =>
            array(
                'CONSULTANT_TYPE_ID' => 1,
                'TYPE_NAME' => 'UTC',
                'TYPE_SCHEME' => 'UTS',
            ),
            1 =>
            array(
                'CONSULTANT_TYPE_ID' => 2,
                'TYPE_NAME' => 'PRC',
                'TYPE_SCHEME' => 'PRS',
            ),
        ));
    }
}
