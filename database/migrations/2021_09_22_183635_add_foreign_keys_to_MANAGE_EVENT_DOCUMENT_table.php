<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMANAGEEVENTDOCUMENTTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MANAGE_EVENT_DOCUMENT', function (Blueprint $table) {
            $table->foreign('MANAGE_EVENT_ID', 'MANAGE_EVENT_DOCUMENT_ibfk_1')->references('MANAGE_EVENT_ID')->on('MANAGE_EVENT')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MANAGE_EVENT_DOCUMENT', function (Blueprint $table) {
            $table->dropForeign('MANAGE_EVENT_DOCUMENT_ibfk_1');
        });
    }
}
