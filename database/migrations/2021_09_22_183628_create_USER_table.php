<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUSERTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('USER', function (Blueprint $table) {
            $table->integer('USER_ID', true);
            $table->string('KEYCLOAK_ID', 100);
            $table->string('USER_NAME', 100);
            $table->string('USER_EMAIL', 150)->nullable();
            $table->integer('USER_CITIZEN');
            $table->string('USER_NRIC', 12);
            $table->date('USER_DOB');
            $table->integer('USER_GROUP');
            $table->timestamp('CREATE_TIMESTAMP')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('USER');
    }
}
