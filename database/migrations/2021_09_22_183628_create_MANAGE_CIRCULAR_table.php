<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMANAGECIRCULARTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MANAGE_CIRCULAR', function (Blueprint $table) {
            $table->integer('MANAGE_CIRCULAR_ID', true);
            $table->integer('MANAGE_DEPARTMENT_ID')->nullable();
            $table->integer('CIRCULAR_STATUS')->comment('	1 - draft; 2 - pending, 3 - Return, 4 - Publish');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MANAGE_CIRCULAR');
    }
}
