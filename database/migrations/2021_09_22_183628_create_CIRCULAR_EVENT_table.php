<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCIRCULAREVENTTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CIRCULAR_EVENT', function (Blueprint $table) {
            $table->integer('CIRCULAR_EVENT_ID', true);
            $table->integer('MANAGE_CIRCULAR_ID');
            $table->string('EVENT_TYPE', 50);
            $table->string('EVENT_TITLE', 100)->nullable();
            $table->string('EVENT_CONTENT', 10000)->nullable();
            $table->date('EVENT_DATE_START')->nullable();
            $table->date('EVENT_DATE_END')->nullable();
            $table->string('EVENT_DISTRIBUTOR_AUDIENCE', 500)->nullable();
            $table->string('EVENT_CONSULTANT_AUDIENCE', 500)->nullable();
            $table->string('EVENT_OTHER_AUDIENCE', 500)->nullable();
            $table->integer('CREATE_BY');
            $table->timestamp('CREATE_TIMESTAMP');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CIRCULAR_EVENT');
    }
}
