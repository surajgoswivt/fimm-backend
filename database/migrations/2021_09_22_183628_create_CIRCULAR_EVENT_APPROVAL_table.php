<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCIRCULAREVENTAPPROVALTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CIRCULAR_EVENT_APPROVAL', function (Blueprint $table) {
            $table->integer('CIRCULAR_EVENT_APPROVAL_ID', true);
            $table->integer('CIRCULAR_EVENT_ID');
            $table->string('APPR_REMARK', 500)->nullable();
            $table->integer('APPR_STATUS')->nullable();
            $table->integer('CREATE_BY')->nullable();
            $table->timestamp('CREATE_TIMESTAMP');
            $table->integer('APPR_GROUP_ID')->nullable();
            $table->integer('APPROVAL_LEVEL_ID')->nullable();
            $table->integer('TS_ID')->nullable();
            $table->integer('APPR_PUBLISH_STATUS')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CIRCULAR_EVENT_APPROVAL');
    }
}
