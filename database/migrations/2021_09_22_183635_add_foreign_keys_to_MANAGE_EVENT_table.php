<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMANAGEEVENTTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MANAGE_EVENT', function (Blueprint $table) {
            $table->foreign('MANAGE_ANNOUNCEMENT_ID', 'MANAGE_EVENT_ibfk_1')->references('MANAGE_ANNOUNCEMENT_ID')->on('MANAGE_ANNOUNCEMENT')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MANAGE_EVENT', function (Blueprint $table) {
            $table->dropForeign('MANAGE_EVENT_ibfk_1');
        });
    }
}
