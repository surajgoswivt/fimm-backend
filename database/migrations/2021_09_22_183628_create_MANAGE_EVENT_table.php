<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMANAGEEVENTTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MANAGE_EVENT', function (Blueprint $table) {
            $table->integer('MANAGE_EVENT_ID', true);
            $table->string('EVENT_TYPE', 100);
            $table->integer('MANAGE_ANNOUNCEMENT_ID')->nullable()->index('MANAGE_ANNOUNCEMENT_ID');
            $table->string('EVENT_TITLE', 100)->nullable();
            $table->string('EVENT_CONTENT', 10000)->nullable();
            $table->date('EVENT_DATE_START')->nullable();
            $table->date('EVENT_DATE_END')->nullable();
            $table->string('EVENT_DISTRIBUTOR_AUDIENCE', 500)->nullable()->comment('Array of Distributor Type Id ');
            $table->string('EVENT_CONSULTANT_AUDIENCE', 500)->nullable()->comment('Array of Consultant Type Id ');
            $table->string('EVENT_OTHER_AUDIENCE', 500)->nullable()->comment('Array of Setting General Id ');
            $table->integer('CREATE_BY')->nullable();
            $table->timestamp('CREATE_TIMESTAMP')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MANAGE_EVENT');
    }
}
