<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\generateDocs::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('command:autoapproval')->daily()->at('22:09')->when(function () {
            return date('Y-m-d') == '2021-05-16';
        });

        // $schedule->command('command:autoapproval')->daily()->at('12:00')->when(function () use ($dateInDatabase) {
        //     return (
        //         $dateInDatabase == Carbon::today() ||
        //         $dateInDatabase == Carbon::yesterday() ||
        //         $dateInDatabase == Carbon::subDays(2)
        //     );
        // });

        // $schedule->command('inspire')->hourly();
        // $schedule->call(function () {
        //     DB::table('recent_users')->delete();
        // })->everyMinute();

        // $schedule->call('App\Http\Controllers\SmsTacController@MyAction')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    
}
