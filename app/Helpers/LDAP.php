<?php

namespace App\Helpers;

use App\Helpers\Decrypt;
use App\Models\KeycloakDefaultGroup;
use App\Models\KeycloakSettings;
use App\Models\PasswordHistory;

use Config;
use Ixudra\Curl\Facades\Curl;

class LDAP
{
    public function testLDAP($request)
    {
        $token = $request->bearerToken();
        $response = Curl::to(env('KEYCLOAK_ADMIN_BASE_URL', 0) . '/testLDAPConnection')
            ->withBearer($token)
            ->withData([
                'action' => 'testAuthentication',
                'authType' => 'simple',
                'bindCredential' => $request->bindCredential,
                'bindDn' => $request->bindDn,
                'connectionUrl' => $request->connectionUrl,
                'startTls' => false,
                'useTruststoreSpi' => 'Only for ldaps'
            ])
            ->post();

        return $response;
    }
}
