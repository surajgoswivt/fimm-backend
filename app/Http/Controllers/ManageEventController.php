<?php

namespace App\Http\Controllers;
use App\Models\DistributorType;
use App\Models\ConsultantType;
use App\Models\SettingGeneral;
use App\Models\ManageAnnouncement;
use App\Models\ManageEvent;
use App\Models\ManageEventDocument;
use App\Models\ManageEventApproval;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\DB;
use Validator;
use Storage;
use File;
use App\Helpers\Files;
use App\Http\Controllers\FileUpload;
use App\Helpers\ManageNotification;
use Hamcrest\Arrays\IsArray;

class ManageEventController extends Controller
{
    public function get(Request $request)
    {
        try {
            $data = ManageEvent::select('MANAGE_EVENT.MANAGE_EVENT_ID','MANAGE_EVENT.EVENT_TITLE',
            'MANAGE_EVENT.EVENT_CONTENT','MANAGE_EVENT.EVENT_DATE_START','MANAGE_EVENT.EVENT_DATE_END',
            'MANAGE_EVENT.MANAGE_EVENT_ID','MANAGE_EVENT.EVENT_DISTRIBUTOR_AUDIENCE','MANAGE_EVENT.EVENT_CONSULTANT_AUDIENCE',
            'MANAGE_EVENT.EVENT_OTHER_AUDIENCE','MANAGE_ANNOUNCEMENT.ANNOUNCEMENT_STATUS','MANAGE_ANNOUNCEMENT.MANAGE_ANNOUNCEMENT_ID')
            ->join('MANAGE_ANNOUNCEMENT', 'MANAGE_ANNOUNCEMENT.MANAGE_ANNOUNCEMENT_ID', '=', 'MANAGE_EVENT.MANAGE_ANNOUNCEMENT_ID')
            ->where('MANAGE_EVENT.MANAGE_EVENT_ID', $request->MANAGE_EVENT_ID)
            ->first();

            // $document = ManageEventDocument::select('*')
            // ->where('MANAGE_EVENT_DOCUMENT.MANAGE_EVENT_ID', $request->MANAGE_EVENT_ID)
            // ->get();

            $distributorAudience = DistributorType::all();
            foreach($distributorAudience as $distAudience){
                $distAudience->setSelected(false);
                foreach(json_decode($data->EVENT_DISTRIBUTOR_AUDIENCE) as $distAudiences){
                    if($distAudience->DISTRIBUTOR_TYPE_ID == $distAudiences){
                        $distAudience->setSelected(true);
                    }
                }
            }

            $consultantAudience = ConsultantType::all();
            foreach($consultantAudience as $consAudience){
                $consAudience->setSelected(false);
                if($data->EVENT_CONSULTANT_AUDIENCE != null){
                    foreach(json_decode($data->EVENT_CONSULTANT_AUDIENCE) as $consAudiences){
                        if($consAudience->CONSULTANT_TYPE_ID == $consAudiences){
                            $consAudience->setSelected(true);
                        }
                    }
                }
            }

            $otherAudience = SettingGeneral::where('SET_TYPE','USERCATEGORY')
            ->where('SET_CODE','other')
            ->get();
            foreach($otherAudience as $OthAudience){
                $OthAudience->setSelected(false);
                if($data->EVENT_OTHER_AUDIENCE != null){
                    foreach(json_decode($data->EVENT_OTHER_AUDIENCE) as $othAudiences){
                        if($OthAudience->SETTING_GENERAL_ID == $othAudiences){
                            $OthAudience->setSelected(true);
                        }
                    }
                }
            }

            $fileReader = new Files();

            $result = $fileReader->getFile($request);

            //  $fileObject = response()->file(storage_path('app/public/event-document/83_29_template example 1.pdf'));
            //  return $fileObject;
            // $destinationPath = storage_path('app/public/test');
            // $result->move($destinationPath, 'test'.$result->getClientOriginalExtension());
             //dd($result);


         //dd($result);
          // return response()->file(storage_path('app/public/event-document/83_29_template example 1.pdf'));
        //   return $result;

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => ([
                    'event' => $data,
                    //'document' => $document,
                    'distributorAudience' =>  $distributorAudience,
                    'consultantAudience' => $consultantAudience,
                    'otherAudience' => $otherAudience
                ]),
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.',
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll(Request $request)
    {
        try {
            $data = ManageEvent::select('MANAGE_EVENT.MANAGE_EVENT_ID','MANAGE_EVENT.EVENT_TITLE','MANAGE_EVENT.EVENT_CONTENT','MANAGE_EVENT.EVENT_DATE_START',
            'MANAGE_EVENT.EVENT_DATE_END', 'USER.USER_NAME',
            'MANAGE_ANNOUNCEMENT.ANNOUNCEMENT_STATUS')
            ->join('MANAGE_ANNOUNCEMENT', 'MANAGE_ANNOUNCEMENT.MANAGE_ANNOUNCEMENT_ID', '=', 'MANAGE_EVENT.MANAGE_ANNOUNCEMENT_ID')
            ->leftJoin('USER', 'USER.USER_ID', '=', 'MANAGE_EVENT.CREATE_BY' )
            //->join('MANAGE_EVENT_DOCUMENT', 'MANAGE_EVENT_DOCUMENT.MANAGE_EVENT_ID', '=', 'MANAGE_EVENT.MANAGE_EVENT_ID')
            // ->join('TASK_STATUS AS STATUS', 'STATUS.TS_PARAM', '=', 'MANAGE_EVENT.CREATE_BY')
            // ->orderBy('MANAGE_EVENT.CREATE_TIMESTAMP', 'desc')
            ->where('MANAGE_EVENT.EVENT_TYPE', $request->EVENT_TYPE)
            ->get();

            foreach($data as $item){
                $item->EVENT_DATE_START = date('d M Y', strtotime($item->EVENT_DATE_START));
                $item->EVENT_DATE_END = date('d M Y', strtotime($item->EVENT_DATE_END));
            }

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.',
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
        try {
            $dataManageAnnouncement = new ManageAnnouncement;
            $dataManageAnnouncement->MANAGE_DEPARTMENT_ID = $request->MANAGE_DEPARTMENT_ID;
            $dataManageAnnouncement->ANNOUNCEMENT_STATUS = $request->ANNOUNCEMENT_STATUS;
            $dataManageAnnouncement->save();

            $dataManageEvent = new ManageEvent;
            $dataManageEvent->MANAGE_ANNOUNCEMENT_ID = $dataManageAnnouncement->MANAGE_ANNOUNCEMENT_ID;
            $dataManageEvent->EVENT_TITLE = $request->EVENT_TITLE;
            $dataManageEvent->EVENT_TYPE = $request->EVENT_TYPE;
            $dataManageEvent->EVENT_CONTENT	 = $request->EVENT_CONTENT;
            $dataManageEvent->EVENT_DATE_START = $request->EVENT_DATE_START;
            $dataManageEvent->EVENT_DATE_END = $request->EVENT_DATE_END;
            $dataManageEvent->EVENT_DISTRIBUTOR_AUDIENCE = $request->EVENT_DISTRIBUTOR_AUDIENCE;
            $dataManageEvent->EVENT_CONSULTANT_AUDIENCE = $request->EVENT_CONSULTANT_AUDIENCE;
            $dataManageEvent->EVENT_OTHER_AUDIENCE = $request->EVENT_OTHER_AUDIENCE;
            $dataManageEvent->CREATE_BY = $request->CREATE_BY;
            $dataManageEvent->save();
            //approval
            foreach(json_decode($request->APPR_LIST) as $item) {
            $approval = new ManageEventApproval;
            $approval->MANAGE_EVENT_ID = $dataManageEvent->MANAGE_EVENT_ID;
            $approval->APPR_GROUP_ID = $item->APPR_GROUP_ID;
            $approval->TS_ID = 15;
            $approval->APPROVAL_LEVEL_ID = $item->APPROVAL_LEVEL_ID;
            $approval->save();

            // Notification
            $notification = new ManageNotification();
            $add = $notification->add($item->APPR_GROUP_ID,$item->APPR_PROCESSFLOW_ID,$request->NOTI_REMARK,$request->NOTI_LOCATION);

        }

            $file = $request->file;
            foreach ($file as $item){
            $itemFile = $item;
            $blob = $item->openFile()->fread($itemFile->getSize());
            $dataManageEventDoc = new ManageEventDocument;
            $dataManageEventDoc->MANAGE_EVENT_ID = $dataManageEvent->MANAGE_EVENT_ID;
            $dataManageEventDoc->DOCUMENT_BLOB = $blob;
            $dataManageEventDoc->DOCUMENT_MIMETYPE = $itemFile->getMimeType();
            $dataManageEventDoc->DOCUMENT_FILENAME	 = $itemFile->getClientOriginalName();
            //$dataManageEventDoc->DOCUMENT_FILEPATH = $destinationPath;
            $dataManageEventDoc->DOCUMENT_FILESIZE = $itemFile->getSize();
            $dataManageEventDoc->CREATE_BY = $request->CREATE_BY;
            $dataManageEventDoc->save();
            }

            http_response_code(200);
            return response([
                'message' => 'Data successfully created.'
            ]);


        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be created.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function manage(Request $request)
    {
$validator = Validator::make($request->all(), [
			'MANAGE_ANNOUNCEMENT_ID' => 'integer|nullable',
			'EVENT_TITLE' => 'string|nullable',
			'EVENT_CONTENT' => 'string|nullable',
			'EVENT_DATE_START' => 'string|nullable',
			'EVENT_DATE_END' => 'string|nullable',
			'EVENT_AUDIENCE' => 'string|nullable',
			'CREATE_BY' => 'integer|nullable',
			'CREATE_TIMESTAMP' => 'integer|nullable'
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request)
    {
        try {
            $dataManageAnnouncement = ManageAnnouncement::find($request->MANAGE_ANNOUNCEMENT_ID);
            $dataManageAnnouncement->MANAGE_DEPARTMENT_ID = $request->MANAGE_DEPARTMENT_ID;
            $dataManageAnnouncement->ANNOUNCEMENT_STATUS = $request->ANNOUNCEMENT_STATUS;
            $dataManageAnnouncement->save();

            $dataManageEvent = ManageEvent::find($request->MANAGE_EVENT_ID);
            $dataManageEvent->MANAGE_ANNOUNCEMENT_ID = $dataManageAnnouncement->MANAGE_ANNOUNCEMENT_ID;
            $dataManageEvent->EVENT_TITLE = $request->EVENT_TITLE;
            $dataManageEvent->EVENT_CONTENT	 = $request->EVENT_CONTENT;
            $dataManageEvent->EVENT_DATE_START = $request->EVENT_DATE_START;
            $dataManageEvent->EVENT_DATE_END = $request->EVENT_DATE_END;
            $dataManageEvent->EVENT_DISTRIBUTOR_AUDIENCE = $request->EVENT_DISTRIBUTOR_AUDIENCE;
            $dataManageEvent->EVENT_CONSULTANT_AUDIENCE = $request->EVENT_CONSULTANT_AUDIENCE;
            $dataManageEvent->EVENT_OTHER_AUDIENCE = $request->EVENT_OTHER_AUDIENCE;
            $dataManageEvent->CREATE_BY = $request->CREATE_BY;
            $dataManageEvent->save();

            foreach(json_decode($request->APPR_LIST) as $item) {
                $approval = new ManageEventApproval;
                $approval->MANAGE_EVENT_ID = $dataManageEvent->MANAGE_EVENT_ID;
                $approval->APPR_GROUP_ID = $item->APPR_GROUP_ID;
                $approval->TS_ID = 15;
                $approval->APPROVAL_LEVEL_ID = $item->APPROVAL_LEVEL_ID;
                $approval->save();

                // Notification
                $notification = new ManageNotification();
                $add = $notification->add($item->APPR_GROUP_ID,$item->APPR_PROCESSFLOW_ID,$request->NOTI_REMARK,$request->NOTI_LOCATION);

            }

            if($request->isFile == 1){
            $file = $request->file;
            foreach ($file as $item){
            $itemFile = $item;
            $blob = $item->openFile()->fread($itemFile->getSize());
            $dataManageEventDoc = new ManageEventDocument;
            $dataManageEventDoc->MANAGE_EVENT_ID = $dataManageEvent->MANAGE_EVENT_ID;
            $dataManageEventDoc->DOCUMENT_BLOB = $blob;
            $dataManageEventDoc->DOCUMENT_MIMETYPE = $itemFile->getMimeType();
            $dataManageEventDoc->DOCUMENT_FILENAME	 = $itemFile->getClientOriginalName();
            //$dataManageEventDoc->DOCUMENT_FILEPATH = $destinationPath;
            $dataManageEventDoc->DOCUMENT_FILESIZE = $itemFile->getSize();
            $dataManageEventDoc->CREATE_BY = $request->CREATE_BY;
            $dataManageEventDoc->save();
            }
            }


            http_response_code(200);
            return response([
                'message' => 'Data successfully created.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete(Request $request)
    {
        try {
            $data = ManageEvent::findorfail($request->MANAGE_EVENT_ID);
            $managementApprovals = ManageEventApproval::where('MANAGE_EVENT_ID', $request->MANAGE_EVENT_ID)->get();
            if(isset($managementApprovals)) {
                foreach ($managementApprovals as $mgmtAppr) {
                    $mgmtAppr->delete();
                }
            }
            $managementDocs = ManageEventDocument::where('MANAGE_EVENT_ID', $request->MANAGE_EVENT_ID)->get();
            if(isset($managementDocs)) {
                foreach ($managementDocs as $mgmtDoc) {
                    $mgmtDoc->delete();
                }
            }
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
$validator = Validator::make($request->all(), [
			'MANAGE_ANNOUNCEMENT_ID' => 'integer|nullable',
			'EVENT_TITLE' => 'string|nullable',
			'EVENT_CONTENT' => 'string|nullable',
			'EVENT_DATE_START' => 'string|nullable',
			'EVENT_DATE_END' => 'string|nullable',
			'EVENT_AUDIENCE' => 'string|nullable',
			'CREATE_BY' => 'integer|nullable',
			'CREATE_TIMESTAMP' => 'integer|nullable'
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
