<?php

namespace App\Http\Controllers;

use App\Models\SettingSms;
use App\Models\SmsLog;
use App\Helpers\SMS;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use Validator;

class SettingSmsController extends Controller
{
    public function get(Request $request)
    {
        try {
			$data = SettingSms::orderBy('SETTING_SMS_ID')->first(); 

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll()
    {
        try {
            $data = SettingSms::first();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
			'SMS_HTTP_URL' => 'string', 
			'SMS_HTTP_PARAM' => 'string', 
			'SMS_REQ_HEADER' => 'string', 
			'SMS_RES_SUCCESS' => 'string', 
			'SMS_RES_FAILURE' => 'string' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }


        try {

            $data = new SettingSms;
            $data->SMS_HTTP_URL = $request->SMS_HTTP_URL;
            $data->SMS_HTTP_PARAM = $request->SMS_HTTP_PARAM;
            $data->SMS_REQ_HEADER = $request->SMS_REQ_HEADER;
            $data->SMS_RES_SUCCESS = $request->SMS_RES_SUCCESS;
            $data->SMS_RES_FAILURE = $request->SMS_RES_FAILURE;
            $data->save();
            //create function

            http_response_code(200);
            return response([
                'message' => 'Data successfully updated.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function test(Request $request)
    {
        try {
            $sms = new SMS();

            $result = $sms->testSend($request);

            if($result->expired)
            {
                return redirect('expired');
            }

            $response = $result->data;

            $responseArray = json_decode($response,true);

            // echo $responseArray['messages'][0]['msgid'];
            // $msgId = $responseArray['messages'][0]['msgid'];
            // {
            //     "messages":[
            //       {
            //         "status": 0,
            //         "receiver": "60173788399",
            //         "msgid": "cust20013050311050614001"
            //       }
            //     ]
            //   }

            $log = new SmsLog();
            $log->SMS_MESSAGE_ID = 1;
            $log->SMS_RECIPIENT = $request->phoneNo;
            $log->SMS_STATUS = 0;
            $log->save();


            http_response_code(200);
            return response([
                'message' => 'SMS successfully tested.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'SMS failed to be tested.',
                'errorCode' => 4104
            ],400);
        }
    }

    public function getLog()
    {
        try {
            $sms = new SMS();

            $result = $sms->getLog();

            if($result->expired)
            {
                return redirect('expired');
            }

            $response = $result->data;


            http_response_code(200);
            return response([
                'message' => $response
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'SMS log failed to be retrieved.',
                'errorCode' => 4104
            ],400);
        }
    }


    public function manage(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
			'SMS_HTTP_URL' => 'string', 
			'SMS_HTTP_PARAM' => 'string', 
			'SMS_REQ_HEADER' => 'string', 
			'SMS_RES_SUCCESS' => 'string', 
			'SMS_RES_FAILURE' => 'string' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [ 
			'SMS_HTTP_URL' => 'string', 
			'SMS_HTTP_PARAM' => 'string', 
			'SMS_REQ_HEADER' => 'string', 
			'SMS_RES_SUCCESS' => 'string', 
			'SMS_RES_FAILURE' => 'string' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            $data = SettingSms::where('id',$id)->first();
            $data->TEST = $request->TEST; //nama column
            $data->save();

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete($id)
    {
        try {
            $data = SettingSms::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
			'SMS_HTTP_URL' => 'string', 
			'SMS_HTTP_PARAM' => 'string', 
			'SMS_REQ_HEADER' => 'string', 
			'SMS_RES_SUCCESS' => 'string', 
			'SMS_RES_FAILURE' => 'string' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
