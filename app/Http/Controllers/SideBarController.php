<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ManageModule;
use App\Models\ManageSubmodule;
use App\Models\ManageScreenAccess;
use App\Models\ManageScreen;
use Illuminate\Http\Request;

class sidebar {
    public $name;
    public $displayName;
    public $meta;
}

class SideBarController extends Controller
{
    public function getSideBarByGroupId(Request $request)
    {
        try {  

            $sidebar = array();
            $subsidebar = array();
            $modules = array();

            $screenIdArray = ManageScreenAccess::where('MANAGE_GROUP_ID', 
                             $request->MANAGE_GROUP_ID)->value('MANAGE_SCREEN_ID');
            
            $screenIdArray = json_decode($screenIdArray,true);

            $current = 0;

            //NANTI BUKAK - TUTUP DULU UNUTK TUTUP ERROR

            // foreach($screenIdArray as $key=>$screenId){

            //     $screen = ManageScreen::find($screenId);
            //     $submoduleId = $screen->MANAGE_SUBMODULE_ID;
            //     $submodule = ManageSubmodule::find($submoduleId);
            //     $moduleId = $submodule->MANAGE_MODULE_ID;

            //     $modules[$moduleId] = $moduleId;

            //     $sub = new sidebar;
            //     $sub->path = $screen->SCREEN_ROUTE;
            //     $sub->displayName = ucwords(strtolower($submodule->SUBMOD_NAME));

            //     $subsidebar[$moduleId][] = $sub;

            //     if ($current == $moduleId) {

            //     } else {
            //         $sub = new sidebar;
            //         $sub->path = 'dashboard-setting/'.$screen->SCREEN_ROUTE;
            //         $sub->displayName = 'Dashboard Setting';
            //         $subsidebar[$moduleId][] = $sub;
            //     }

            //     $current = $moduleId;
            // }

            // foreach ($modules as $moduleId) {
            //     $module = ManageModule::find($moduleId);

            //     $s = new sidebar;
            //     $s->order = $module->MOD_INDEX;
            //     $s->path = $screen->SCREEN_ROUTE;
            //     $s->displayName = ucwords(strtolower($module->MOD_NAME));
            //     $s->meta['iconClass'] = $module->MOD_ICON;
            //     $s->disabled = true;
            //     $s->children = $subsidebar[$moduleId];

            //     $sidebar[] = $s;
            // }


            // dd($sidebar);
            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $sidebar,
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.',
                'errorCode' => 0,
            ]);
        }
    }

    public function getAll()
    {
        try {
            $data = ManageModule::all();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data,
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.',
                'errorCode' => 4103,
            ], 400);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'MOD_CODE' => 'required|string',
            'MOD_NAME' => 'required|string',
            'MOD_SNAME' => 'required|string',
            'MOD_INDEX' => 'required|integer',
            'MOD_ICON' => 'required|string',
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106,
            ], 400);
        }

        try {
            $module = new ManageModule;
            $module->MOD_CODE = $request->MOD_CODE;
            $module->MOD_NAME = $request->MOD_NAME;
            $module->MOD_SNAME = $request->MOD_SNAME;
            $module->MOD_INDEX = $request->MOD_INDEX;
            $module->MOD_ICON = $request->MOD_ICON;
            $module->save();

            http_response_code(200);
            return response([
                'message' => 'Data successfully created.',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be created.',
                'errorCode' => 4100,
            ], 400);
        }

    }

    public function update($id)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        try {
            //update function

            http_response_code(200);
            return response([
                'message' => '',
                'data' => '',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 0,
            ]);
        }
    }

    public function delete($id)
    {
        try {

            $data = ManageModule::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 0,
            ]);
        }
    }
}
