<?php

namespace App\Http\Controllers;

use App\Mail\NewUserNotification;
use App\Mail\casEmailNotification;
use App\Mail\casEmailBarring;
use App\Mail\acceptanceEmail;
use App\Mail\TPNewUserNotification;
use App\Mail\distributorRegistrationApprovalEmail;
use App\Mail\suspendRevokeCeoEmail;
use App\Mail\cessationEmail;
use App\Models\SettingEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

class SettingEmailController extends Controller
{
    public function get()
    {
        $email = Email::first();

        http_response_code(200);
        return response([
            'message' => 'Email successfully retrieved.',
            'data' => $email
        ]);
    }

    public function manage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'EMAIL_SMTP_PORT' => 'required|string', //587
            'EMAIL_SMTP_SERVER' => 'string', //smtp.gmail.com
            'EMAIL_FROM' => 'required|string', //fimm.demo@gmail.com
            'EMAIL_LOGIN_ID' => 'required|string' //pyhupykajwokvagp
        ]);

        try {
            $model = Email::first();
            $model->EMAIL_SMTP_PORT = $request->EMAIL_SMTP_PORT;
            $model->EMAIL_SMTP_SERVER = $request->EMAIL_SMTP_SERVER;
            $model->EMAIL_FROM = $request->EMAIL_FROM;
            $model->EMAIL_LOGIN_ID = $request->EMAIL_LOGIN_ID;
            $model->save();

            http_response_code(200);
            return response([
                'message' => 'Email successfully configured.'
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email connection failed to be configured.',
                'errorCode' => 4001
            ]);
        }
    }

    public function test()
    {
        $email = Email::first();

        http_response_code(200);
        return response([
            'message' => 'Email successfully retrieved.',
            'data' => $email
        ]);
    }

    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

            $otp = substr(str_shuffle(str_repeat($pool, 5)), 0, 16);

            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'name' => $request->name,
                'otp' => $otp,
                'loginUrl' => $request->loginUrl
            );

            Mail::to($data['email'])->send(new NewUserNotification($data));

            http_response_code(200);
            return response([
                'message' => 'Email successfully send.',
                'data' => $otp
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendTPEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

            $otp = substr(str_shuffle(str_repeat($pool, 5)), 0, 16);

            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'name' => $request->name,
                'otp' => $otp,
                'loginUrl' => $request->loginUrl
            );

            Mail::to($data['email'])->send(new TPNewUserNotification($data));

            http_response_code(200);
            return response([
                'message' => 'Email successfully send.',
                'data' => $otp
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendCasEmail(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {

            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'title' => $request->title,
                'userName' => $request->userName,
                'consultantName' =>  $request->consultantName,
                'consultantNric' =>  $request->consultantNric,
                'consultantPassport' =>  $request->consultantPassport,
                'caRemark' =>  $request->caRemark,
                'caComment' => $request->caComment,
            );
            Mail::to($data['email'])->send(new casEmailNotification($data));

            http_response_code(200);
            return response([
                'message' => 'Email Notification successfully send.'

            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email Notification failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendCasBarring(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {

            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'title' => $request->title,
                'userName' => $request->userName,
                'consultantName' =>  $request->consultantName,
                'consultantNric' =>  $request->consultantNric,
                'consultantPassport' =>  $request->consultantPassport,
                'caRemark' =>  $request->caRemark,
                'caEndDate' => $request->caEndDate,
            );
            Mail::to($data['email'])->send(new casEmailBarring($data));

            http_response_code(200);
            return response([
                'message' => 'Email Notification successfully send.'

            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email Notification failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendAcceptanceEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {
            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'name' => $request->name,
                'nric' => $request->nric,
                'passportNo' => $request->passportNo,
                'phoneNo' => $request->phoneNo,
                'licenseType' => $request->licenseType,
                'staffOrAgent' => $request->staffOrAgent,
                'distName' => $request->distName,
                'title' => $request->title

            );
            Mail::to($data['email'])->send(new acceptanceEmail($data));

            http_response_code(200);
            return response([
                'message' => 'Email Notification successfully send.'


            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email Notification failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendDistRegEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);
        try {

            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'distName' => $request->distName,
                'distRemark' => $request->distRemark
            );
            Mail::to($data['email'])->send(new distributorRegistrationApprovalEmail($data));

            http_response_code(200);
            return response([
                'message' => 'Email Notification successfully send.'
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email Notification failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendsuspendRevokeCeoEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {
            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'name' => $request->name,
                'distName' => $request->distName,
                'distRegNo' => $request->distRegNo,
                'distNewRegNo' => $request->distNewRegNo,
                'submissionType' => $request->submissionType,
                'dateStart' => $request->dateStart,
                'dateEnd' => $request->dateEnd,
                'effectiveDate' => $request->effectiveDate,
                'reason' => $request->reason,
                'title' => $request->title
            );

            Mail::to($data['email'])->send(new suspendRevokeCeoEmail($data));

            http_response_code(200);
            return response([
                'message' => 'Email Notification successfully send.'


            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email Notification failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }

    public function sendCeaseEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email' //Hello there
        ]);

        try {
            $settings = SettingEmail::all();
            $data = array(
                'email' => $request->email,
                'name' => $request->name,
                'distName' => $request->distName,
                'distRegNo' => $request->distRegNo,
                'distNewRegNo' => $request->distNewRegNo,
                'cessationName' => $request->cessationName,
                'cessationDate' => $request->cessationDate,
                'title' => $request->title

            );

            Mail::to($data['email'])->send(new cessationEmail($data));

            http_response_code(200);
            return response([
                'message' => 'Email Notification successfully send.'


            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Email Notification failed to be sent.',
                'errorCode' => 4000
            ]);
        }
    }
}
