<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use App\Models\TpScreenAccess;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class TpScreenAccessController extends Controller
{
    public function get(Request $request)
    {
        try {
            $data = TpScreenAccess::find($request->TP_SCREEN_ACCESS_ID);

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAuthorisationPage(Request $request)
    {
        try {
            $data = DB::table('TP_SCREEN_ACCESS AS SCREEN_ACCESS')
            ->select('*')
            ->leftJoin('TP_MANAGE_GROUP AS GROUP', 'GROUP.TP_MANAGE_GROUP_ID','=', 'SCREEN_ACCESS.TP_MANAGE_GROUP_ID')
            ->leftJoin('AUTHORIZATION_LEVEL AS AUTH_LEVEL', 'AUTH_LEVEL.AUTHORIZATION_LEVEL_ID', '=', 'SCREEN_ACCESS.TP_AUTHORISATION_ID')
            ->get();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll()
    {
        try {
            $data = TpScreenAccess::select('*')
            ->leftJoin('TP_MANAGE_GROUP', 'TP_MANAGE_GROUP.TP_MANAGE_GROUP_ID', '=', 'TP_SCREEN_ACCESS.MANAGE_GROUP_ID')
           ->leftJoin('AUTHORIZATION_LEVEL', 'AUTHORIZATION_LEVEL.AUTHORIZATION_LEVEL_ID', '=', 'TP_SCREEN_ACCESS.AUTHORISATION_ID')
           ->get();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
        

        try {
            $data = new TpScreenAccess;
            $data->TP_MANAGE_GROUP_ID = $request->TP_MANAGE_GROUP_ID;
            $data->TP_AUTHORISATION_ID = $request->TP_AUTHORISATION_ID;
            $data->TP_MANAGE_SCREEN_ID = $request->TP_MANAGE_SCREEN_ID;
            $data->TP_USER_ID = $request->TP_USER_ID;
            $data->save();
            //create function

            http_response_code(200);
            return response([
                'message' => 'Data successfully updated.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function manage(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'TP_MANAGE_GROUP_ID' => 'integer|nullable', 
			'TP_AUTHORISATION_ID' => 'integer|nullable', 
			'TP_MANAGE_SCREEN_ID' => 'string|nullable', 
			'TP_USER_ID' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'TP_MANAGE_GROUP_ID' => 'integer|nullable', 
			'TP_AUTHORISATION_ID' => 'integer|nullable', 
			'TP_MANAGE_SCREEN_ID' => 'string|nullable', 
			'TP_USER_ID' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            $data = TpScreenAccess::find($request->TP_SCREEN_ACCESS_ID);
            $data->TP_MANAGE_GROUP_ID = $request->TP_MANAGE_GROUP_ID;
            $data->TP_AUTHORISATION_LEVEL_ID = $request->TP_AUTHORISATION_LEVEL_ID;
            $data-save();

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete($id)
    {
        try {
            $data = TpScreenAccess::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'TP_MANAGE_GROUP_ID' => 'integer|nullable', 
			'TP_AUTHORISATION_ID' => 'integer|nullable', 
			'TP_MANAGE_SCREEN_ID' => 'string|nullable', 
			'TP_USER_ID' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
