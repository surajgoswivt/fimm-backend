<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use App\Models\CircularEvent;
use App\Models\CircularEventDocument;
use App\Models\ManageCircular;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models\CircularEventApproval;
use App\Helpers\ManageNotification;

class CircularEventController extends Controller
{ 
    public function get(Request $request)
    {
        try {
            $data = CircularEvent::select('CIRCULAR_EVENT.CIRCULAR_EVENT_ID','CIRCULAR_EVENT.EVENT_TITLE',
            'CIRCULAR_EVENT.EVENT_CONTENT','CIRCULAR_EVENT.EVENT_DATE_START','CIRCULAR_EVENT.EVENT_DATE_END',
            'CIRCULAR_EVENT.CIRCULAR_EVENT_ID','CIRCULAR_EVENT.EVENT_DISTRIBUTOR_AUDIENCE','CIRCULAR_EVENT.EVENT_CONSULTANT_AUDIENCE',
            'CIRCULAR_EVENT.EVENT_OTHER_AUDIENCE','MANAGE_CIRCULAR.CIRCULAR_STATUS','MANAGE_CIRCULAR.MANAGE_CIRCULAR_ID')
            ->join('MANAGE_CIRCULAR', 'MANAGE_CIRCULAR.MANAGE_CIRCULAR_ID', '=', 'CIRCULAR_EVENT.MANAGE_CIRCULAR_ID')
            ->where('CIRCULAR_EVENT.CIRCULAR_EVENT_ID', $request->CIRCULAR_EVENT_ID)
            ->get();

            $distributorAudience = DistributorType::all();
            foreach($distributorAudience as $distAudience){
                $distAudience->setSelected(false);
                foreach(json_decode($data->EVENT_DISTRIBUTOR_AUDIENCE) as $distAudiences){
                    if($distAudience->DISTRIBUTOR_TYPE_ID == $distAudiences){
                        $distAudience->setSelected(true);
                    }
                }
            }

            $consultantAudience = ConsultantType::all();
            foreach($consultantAudience as $consAudience){
                $consAudience->setSelected(false);
                if($data->EVENT_CONSULTANT_AUDIENCE != null){
                    foreach(json_decode($data->EVENT_CONSULTANT_AUDIENCE) as $consAudiences){
                        if($consAudience->CONSULTANT_TYPE_ID == $consAudiences){
                            $consAudience->setSelected(true);
                        }
                    }
                }
            }

            $otherAudience = SettingGeneral::where('SET_TYPE','USERCATEGORY')
            ->where('SET_CODE','other')
            ->get();
            foreach($otherAudience as $OthAudience){
                $OthAudience->setSelected(false);
                if($data->EVENT_OTHER_AUDIENCE != null){
                    foreach(json_decode($data->EVENT_OTHER_AUDIENCE) as $othAudiences){
                        if($OthAudience->SETTING_GENERAL_ID == $othAudiences){
                            $OthAudience->setSelected(true);
                        }
                    }
                }
            }

            $fileReader = new Files();

            $result = $fileReader->getFile($request);

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data,
                'event' => $event, 
                    'document' =>$document,
                    'distributorAudience' =>  $distributorAudience,
                    'consultantAudience' => $consultantAudience,
                    'otherAudience' => $otherAudience
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll(Request $request)
    {
        try {
            $data = DB::table('admin_management.CIRCULAR_EVENT AS CE')
            ->select('CE.CIRCULAR_EVENT_ID','CE.EVENT_TITLE','CE.EVENT_CONTENT','CE.EVENT_DATE_START','CE.EVENT_DATE_END',
            'CE.TS_ID','TS.TS_PARAM','USER.USER_NAME','CE.CREATE_TIMESTAMP')

            ->leftJoin('admin_management.USER', 'USER.USER_ID', '=', 'CE.CREATE_BY')
            ->leftJoin('admin_management.TASK_STATUS AS TS','TS.TS_ID', '=', 'CE.TS_ID')

            ->where('CE.DEPARTMENT', $request->DEPARTMENT)
            ->get();



            foreach($data as $item){
                 if($item->CREATE_TIMESTAMP != null || $item->CREATE_TIMESTAMP != ""){
                    $item->CREATE_TIMESTAMP = date('d-M-Y', strtotime($item->CREATE_TIMESTAMP));
                }else{
                $item->CREATE_TIMESTAMP = '-';
                }

                if($item->EVENT_DATE_END != null || $item->EVENT_DATE_END != ""){
                    $item->EVENT_DATE_END = date('d-M-Y', strtotime($item->EVENT_DATE_END));
                }else{
                $item->EVENT_DATE_END = '-';
                }

                
                if($item->EVENT_DATE_START != null || $item->EVENT_DATE_START != ""){
                    $item->EVENT_DATE_START = date('d-M-Y', strtotime($item->EVENT_DATE_START));
                }else{
                $item->EVENT_DATE_START = '-';
                }
                
            }


            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function createNewCircular(Request $request)
    {
    
        try {

            $dataCircularEvent = new CircularEvent;
            $dataCircularEvent->EVENT_TITLE = $request->EVENT_TITLE;
            $dataCircularEvent->DEPARTMENT = $request->DEPARTMENT;
            $dataCircularEvent->EVENT_CONTENT = $request->EVENT_CONTENT;
            $dataCircularEvent->EVENT_DATE_START = $request->EVENT_DATE_START;
            $dataCircularEvent->EVENT_DISTRIBUTOR_AUDIENCE = $request->EVENT_DISTRIBUTOR_AUDIENCE;
            $dataCircularEvent->CREATE_BY = $request->CREATE_BY;
            $dataCircularEvent->TS_ID = $request->TS_ID;
            $dataCircularEvent->PUBLISH_STATUS = $request->PUBLISH_STATUS;
            $dataCircularEvent->save();

            if($request->PUBLISH_STATUS == "1"){
            foreach(json_decode($request->APPR_LIST) as $item) {
                $approval = new CircularEventApproval;
                $approval->CIRCULAR_EVENT_ID = $dataCircularEvent->CIRCULAR_EVENT_ID;
                $approval->APPR_GROUP_ID = $item->APPR_GROUP_ID;
                $approval->TS_ID = 15; 
                $approval->APPROVAL_LEVEL_ID = $item->APPROVAL_LEVEL_ID;
                $approval->save();

                $notification = new ManageNotification();
                $add = $notification->add($item->APPR_GROUP_ID, $item->APPR_PROCESSFLOW_ID,$request->NOTI_REMARK, $request->NOTI_LOCATION);
            }

           }

            $file = $request->file;
            foreach ($file as $item){            
            $itemFile = $item;
            $blob = $item->openFile()->fread($itemFile->getSize());
            $dataManageEventDoc = new CircularEventDocument;
            $dataManageEventDoc->CIRCULAR_EVENT_ID = $dataCircularEvent->CIRCULAR_EVENT_ID;
            $dataManageEventDoc->DOC_BLOB = $blob;
            $dataManageEventDoc->DOC_MIMETYPE = $itemFile->getMimeType();
            $dataManageEventDoc->DOC_ORIGINAL_NAME = $itemFile->getClientOriginalName();
            $dataManageEventDoc->DOC_FILESIZE = $itemFile->getSize();
            $dataManageEventDoc->save();
            }
            //create function
            http_response_code(200);
            return response([
                'message' => 'Data successfully created.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function manage(Request $request)
    {
        $validator = Validator::make($request->all(), [ //fresh
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function
            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request)
    {
        
        try {
            $dataCircular =  ManageCircular::find($request->MANAGE_CIRCULAR_ID);
            $dataCircular->MANAGE_DEPARTMENT_ID = $request->MANAGE_DEPARTMENT_ID;
            $dataCircular->CIRCULAR_STATUS = $request->CIRCULAR_STATUS;
            $dataCircular->save();

            $dataCircularEvent = CircularEvent::find($request->CIRCULAR_EVENT_ID);
            $dataCircularEvent->MANAGE_CIRCULAR_ID = $dataCircular->MANAGE_CIRCULAR_ID;
            $dataCircularEvent->EVENT_TITLE = $request->EVENT_TITLE;
            $dataCircularEvent->EVENT_TYPE = $request->EVENT_TYPE;
            $dataCircularEvent->EVENT_CONTENT	 = $request->EVENT_CONTENT;
            $dataCircularEvent->EVENT_DATE_START = $request->EVENT_DATE_START;
            $dataCircularEvent->EVENT_DATE_END = $request->EVENT_DATE_END;
            $dataCircularEvent->EVENT_DISTRIBUTOR_AUDIENCE = $request->EVENT_DISTRIBUTOR_AUDIENCE;
            $dataCircularEvent->EVENT_CONSULTANT_AUDIENCE = $request->EVENT_CONSULTANT_AUDIENCE;
            $dataCircularEvent->EVENT_OTHER_AUDIENCE = $request->EVENT_OTHER_AUDIENCE;
            $dataCircularEvent->CREATE_BY = $request->CREATE_BY;
            $dataCircularEvent->save();

            if($request->isFile == 1){
            $file = $request->file;
            foreach ($file as $item){            
            $itemFile = $item;
            $blob = $item->openFile()->fread($itemFile->getSize());
            $dataManageEventDoc = new CircularEventDocument;
            $dataManageEventDoc->CIRCULAR_EVENT_ID = $dataCircularEvent->CIRCULAR_EVENT_ID;
            $dataManageEventDoc->DOC_BLOB = $blob;
            $dataManageEventDoc->DOC_MIMETYPE = $itemFile->getMimeType();
            $dataManageEventDoc->DOC_ORIGINAL_NAME	 = $itemFile->getClientOriginalName();
            //$dataManageEventDoc->DOC_FILEPATH = $destinationPath;
            $dataManageEventDoc->DOC_FILESIZE = $itemFile->getSize();
            $dataManageEventDoc->CREATE_BY = $request->CREATE_BY;
            $dataManageEventDoc->save();
            }
        }


        } 
        catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete($id)
    {
        try {
            $data = CircularEvent::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
        $validator = Validator::make($request->all(), [ //fresh
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function
            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}