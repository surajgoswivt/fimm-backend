<?php

namespace App\Http\Controllers;

use App\Models\CpdModule;
use Illuminate\Http\Request;

class CpdModuleController extends Controller
{
    public function get($id)
    {
        try {
            $data = App\Models\CpdModule::find($id);

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 0
            ]);
        }
    }

    public function getAll()
    {
        try {
            $data = CpdModule::all();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 0
            ]);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        try {
            //create function

            http_response_code(200);
            return response([
                'message' => 'Data successfully updated.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 0
            ]);
        }

    }

    public function update($id)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        try {
            //update function

            http_response_code(200);
            return response([
                'message' => '',
                'data' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 0,
            ]);
        }
    }

    public function delete($id)
    {
        try {

            $data = CpdModule::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 0
            ]);
        }
    }
}
