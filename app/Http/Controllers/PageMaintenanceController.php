<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use App\Models\PageMaintenance;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class PageMaintenanceController extends Controller
{
    public function get(Request $request)
    {
        try {
            $data = PageMaintenance::find($request->PAGE_MAINTENANCE_ID);

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }
    public function getAudience(Request $request)
    {
        try {
            $data = DB::table('SETTING_GENERAL')
            ->select('*')
            ->where('SET_TYPE', 'AUDIENCE')
            ->get();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll()
    {
        try {
            $data = PageMaintenance::select('*', 'PAGE_MAINTENANCE.MAINTENANCE_START_DATE AS START_DATE', 'PAGE_MAINTENANCE.MAINTENANCE_END_DATE AS END_DATE')
            ->leftJoin('USER', 'USER.USER_ID' , '=', 'PAGE_MAINTENANCE.CREATION_BY')
            ->leftJoin('SETTING_GENERAL', 'SETTING_GENERAL.SETTING_GENERAL_ID', '=', 'PAGE_MAINTENANCE.AUDIENCE')
            ->get();

            foreach($data as $element){
                $element->START_DATE = date('d-M-Y', strtotime($element->MAINTENANCE_START_DATE));
                $element->END_DATE = date('d-M-Y', strtotime($element->MAINTENANCE_END_DATE));
            }

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
    
        try {
            $data = new PageMaintenance;
            $data->MAINTENANCE_START_DATE = $request->MAINTENANCE_START_DATE;
            $data->MAINTENANCE_END_DATE = $request->MAINTENANCE_END_DATE;
            $data->NOTIFICATION_DESC = strtoupper($request->NOTIFICATION_DESC);
            $data->AUDIENCE = $request->AUDIENCE;
            $data->MAINTENANCE_MODULE = $request->MAINTENANCE_MODULE;
            $data->CREATION_BY = $request->CREATION_BY;
            $data->save();
            //create function

            http_response_code(200);
            return response([
                'message' => 'Data successfully updated.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function manage(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'MAINTENANCE_START_DATE' => 'required|integer', 
			'MAINTENANCE_END_DATE' => 'required|integer', 
			'NOTIFICATION_DESC' => 'required|string', 
			'AUDIENCE' => 'required|integer', 
			'MAINTENANCE_MODULE' => 'required|string', 
			'CREATION_BY' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request)
    {
       
        try {
            $data = PageMaintenance::find($request->PAGE_MAINTENANCE_ID);
            $data->MAINTENANCE_START_DATE = $request->MAINTENANCE_START_DATE;
            $data->MAINTENANCE_END_DATE = $request->MAINTENANCE_END_DATE;
            $data->NOTIFICATION_DESC = strtoupper($request->NOTIFICATION_DESC);
            $data->AUDIENCE = $request->AUDIENCE;
            $data->MAINTENANCE_MODULE = $request->MAINTENANCE_MODULE;
            $data->CREATION_BY = $request->CREATION_BY;
            $data->save();

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete(Request $request)
    {
        try {
            $data = PageMaintenance::find($request->PAGE_MAINTENANCE_ID);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'MAINTENANCE_START_DATE' => 'required|integer', 
			'MAINTENANCE_END_DATE' => 'required|integer', 
			'NOTIFICATION_DESC' => 'required|string', 
			'AUDIENCE' => 'required|integer', 
			'MAINTENANCE_MODULE' => 'required|string', 
			'CREATION_BY' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
