<?php

namespace App\Http\Controllers;

use App\Models\ManageScreenAccess;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use Validator;
use DB;

class ManageScreenAccessController extends Controller
{
    public function get(Request $request)
    {
        try {
			$data = ManageScreenAccess::find($request->MANAGE_SCREEN_ACCESS_ID); 

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }
    public function getUser(Request $request)
    {
        try {
			$data = DB::table('USER')
            ->select('*')
            ->get();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAuthorization(Request $request)
    {
        try {
			$data = DB::table('AUTHORIZATION_LEVEL')
            ->select('*')
            ->get();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }
    public function getAllGroup(Request $request)
    {
        try {
			$data = DB::table('MANAGE_GROUP')
            ->select('*')
            ->get();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }
    public function getAll()
    {
        try {
            $data = ManageScreenAccess::select('*')
           ->leftJoin('MANAGE_GROUP', 'MANAGE_GROUP.MANAGE_GROUP_ID', '=', 'MANAGE_SCREEN_ACCESS.MANAGE_GROUP_ID')
           ->leftJoin('AUTHORIZATION_LEVEL', 'AUTHORIZATION_LEVEL.AUTHORIZATION_LEVEL_ID', '=', 'MANAGE_SCREEN_ACCESS.AUTHORIZATION_LEVEL_ID')
           ->get();
           $total = $data->count();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data,
                'count' => $total
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
			'MANAGE_GROUP_ID' => 'required|integer', 
			'AUTHORIZATION_LEVEL_ID' => 'required|integer',
            'MANAGE_SCREEN_ID' => 'required|string',
            'USER_ID' =>  'required|integer'
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            $screen = new ManageScreenAccess;
            $screen->MANAGE_GROUP_ID = $request->MANAGE_GROUP_ID;
            $screen->AUTHORIZATION_LEVEL_ID = $request->AUTHORIZATION_LEVEL_ID;
            $screen->MANAGE_SCREEN_ID = $request->MANAGE_SCREEN_ID;
            $screen->USER_ID = $request->USER_ID;
            $screen->save();

            http_response_code(200);
            return response([
                'message' => 'Data successfully updated.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function manage(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'MANAGE_GROUP_ID' => 'required|integer', 
			'AUTHORIZATION_LEVEL_ID' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'MANAGE_GROUP_ID' => 'required|integer', 
			'AUTHORIZATION_LEVEL_ID' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            $data = ManageScreenAccess::find($request->MANAGE_SCREEN_ACCESS_ID);
            $screen->MANAGE_GROUP_ID = $request->MANAGE_GROUP_ID;
            $screen->AUTHORIZATION_LEVEL_ID = $request->AUTHORIZATION_LEVEL_ID;
            $screen->MANAGE_SCREEN_ID = $request->MANAGE_SCREEN_ID;
            $screen->USER_ID = $request->$request->USER_ID;
            $screen->save();

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete($id)
    {
        try {
            $data = ManageScreenAccess::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
$validator = Validator::make($request->all(), [ 
			'MANAGE_GROUP_ID' => 'required|integer', 
			'AUTHORIZATION_LEVEL_ID' => 'required|integer' 
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
