<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use LaravelKeycloakAdmin\Facades\KeycloakAdmin;
use Validator;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class FiMMUserController extends Controller
{
    //
    public function get(Request $request)
    {
    //     return KeycloakAdmin::user()->find([
    //         'query' => [
    //              'username' => $request->login_id
    //         ]
    //    ]);
    }

    public function getAll(Request $request)
    {
        // return KeycloakAdmin::user()->all();
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login_id' => 'string', //dummy1
            'email' => 'string|email' //dummy1@vn.my
        ]);

        // return KeycloakAdmin::user()->all();
    }

    public function delete(Request $request)
    {
        return $request->login_id;
        // return KeycloakAdmin::user()->all();
    }
}
