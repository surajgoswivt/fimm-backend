<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ManageModule;
use Illuminate\Http\Request;
use Validator;

class ManageModuleController extends Controller
{
    public function get(Request $request)
    {
        try {
            $data = ManageModule::find($request->MANAGE_MODULE_ID);

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data,
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.',
                'errorCode' => 0,
            ]);
        }
    }

    public function getAll()
    {
        try {
            // $data = ManageModule::all();
            $data = ManageModule::orderBy('MOD_INDEX', 'asc')
            // ->orderBy('MOD_INDEX', 'asc')
            ->get();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data,
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.',
                'errorCode' => 4103,
            ], 400);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'MOD_CODE' => 'required|string',
            'MOD_NAME' => 'required|string',
            'MOD_SNAME' => 'required|string',
            'MOD_INDEX' => 'required|integer',
            'MOD_ICON' => 'required|string'
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106,
            ], 400);
        }

        try {
            $module = new ManageModule;
            $module->MOD_CODE = $request->MOD_CODE;
            $module->MOD_NAME = strtoupper($request->MOD_NAME);
            $module->MOD_SNAME = strtoupper($request->MOD_SNAME);
            $module->MOD_INDEX = $request->MOD_INDEX;
            $module->MOD_ICON = $request->MOD_ICON;
            $module->save();

            http_response_code(200);
            return response([
                'message' => 'Data successfully created.',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be created.',
                'errorCode' => 4100,
            ], 400);
        }

    }

    public function update(Request $request)
    {
    
        try {
            $module = ManageModule::find($request->MANAGE_MODULE_ID);
            $module->MOD_CODE = $request->MOD_CODE;
            $module->MOD_NAME = strtoupper($request->MOD_NAME);
            $module->MOD_SNAME = strtoupper($request->MOD_SNAME);
            $module->MOD_INDEX = $request->MOD_INDEX;
            $module->MOD_ICON = $request->MOD_ICON;
            $module->save();
            //update function

            http_response_code(200);
            return response([
                'message' => '',
                'data' => '',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 0,
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            $data = ManageModule::find($request->MANAGE_MODULE_ID);
            $data->delete();



            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 0,
            ]);
        }
    }
}
