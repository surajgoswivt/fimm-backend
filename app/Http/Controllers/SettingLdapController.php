<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use App\Models\SettingLdap;
use App\Helpers\LDAP;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
// use LaravelKeycloakAdmin\Facades\KeycloakAdmin;

class SettingLdapController extends Controller
{
    public function get()
    {
        try {
            $data = SettingLdap::first();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll()
    {
        try {
            $data = SettingLdap::all();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //create function

            http_response_code(200);
            return response([
                'message' => 'Data successfully updated.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function test(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bindDn' => 'required|string', //CN=dummy,OU=HR Admin,DC=ad,DC=vn,DC=my
            'bindCredential' => 'required|string', //@Bcd1234
            'connectionUrl' => 'required|string' //ldap://192.168.3.199:389
        ]);

        try {
            $ldap = new LDAP;
            $testLDAP = $ldap->testLDAP($request);
            // $ldap = KeycloakAdmin::addon()->testLDAPConnection([
            //     'body' => [
            //         'action' => 'testAuthentication',
            //         'authType' => 'simple',
            //         'bindCredential' => $request->bindCredential,
            //         'bindDn' => $request->bindDn,
            //         'connectionUrl' => $request->connectionUrl,
            //         'startTls' => false,
            //         'useTruststoreSpi' => 'Only for ldaps',
            //     ],
            // ]);
            dd($testLDAP);

            http_response_code(200);
            return response([
                'message' => 'LDAP successfully tested and able to connect.',
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'LDAP connection is not successful.',
                'errorCode' => 4005
            ]);
        }

    }

    public function manage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function
            $model = SettingLdap::first();
            // $model->EMAIL_SMTP_PORT = $request->EMAIL_SMTP_PORT;
            // $model->EMAIL_SMTP_SERVER = $request->EMAIL_SMTP_SERVER;
            // $model->EMAIL_FROM = $request->EMAIL_FROM;
            // $model->EMAIL_LOGIN_ID = $request->EMAIL_LOGIN_ID;
            $model->LDAP_ATTR_RDN = $request->LDAP_ATTR_RDN; // cn
            $model->LDAP_ATTR_UUID = $request->LDAP_ATTR_UUID; // objectGUID
            $model->LDAP_USER_OBJ = $request->LDAP_USER_OBJ; // person,organisationalPerson,user
            $model->LDAP_USER_URL = $request->LDAP_USER_URL; // ldap://192.163.3.199:389
            $model->LDAP_USER_DN = $request->LDAP_USER_DN; // OU=HR Admin,DC=ad,DC=vn,DC=my
            $model->LDAP_USER_FILTER = $request->LDAP_USER_FILTER; // LDAP Filter
            $model->LDAP_SEARCH_SCOPE = $request->LDAP_SEARCH_SCOPE; // One Level
            $model->LDAP_BIND_TYPE = $request->LDAP_BIND_TYPE; // simple
            $model->LDAP_BIND_DN = $request->LDAP_BIND_DN; // CN=dummy,OU=HR Admin,DC=ad,DC=vn,DC=my
            $model->LDAP_BIND_CRED = $request->LDAP_BIND_CRED; // @Bcd1234
            $model->save();

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            $data = SettingLdap::where('id',$id)->first();
            $data->TEST = $request->TEST; //nama column
            $data->save();

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete($id)
    {
        try {
            $data = SettingLdap::find($id);
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
