<?php

namespace App\Http\Controllers;

use App\Models\KeycloakSettings;
use App\Models\ManageGroup;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
// use LaravelKeycloakAdmin\Facades\KeycloakAdmin;
use App\Helpers\CurrentUser;
use Validator;
use Auth;
use DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|string|max:255', //reg-client
            'login_id' => 'required|string', //dummy
            'password' => 'required|string' //@Bcd1234
        ]);

        $setting = KeycloakSettings::where('KEYCLOAK_CLIENT_ID', $request->client_id)
            ->first();

        $response = Curl::to($setting->KEYCLOAK_TOKEN_URL)

            ->withData([
                'username' => $request->login_id,
                'password' => $request->password,
                'client_id' => $setting->KEYCLOAK_CLIENT_ID,
                'grant_type' => 'password',
                'client_secret' => $setting->KEYCLOAK_CLIENT_SECRET,
            ])
            ->post();

        $checkLoginId = '';

        $response = json_decode($response, true);
        if (isset($response['error']) == "invalid_grant") {

            http_response_code(400);
            return response([
                'message' => 'Invalid login credentials.',
                'errorCode' => 4003
            ],400);

        } else {
            $CurrentUser = new CurrentUser();
            $user = $CurrentUser->getUserDetails($response['access_token']);
            $userdetail = DB::table('admin_management.USER AS user')
            ->join('admin_management.MANAGE_GROUP AS group', 'group.MANAGE_GROUP_ID', '=', 'user.USER_GROUP')
            ->join('admin_management.MANAGE_DEPARTMENT AS department', 'department.MANAGE_DEPARTMENT_ID', '=', 'group.MANAGE_DEPARTMENT_ID')
            ->join('admin_management.MANAGE_DIVISION AS division', 'division.MANAGE_DIVISION_ID', '=', 'department.MANAGE_DIVISION_ID')
            ->where('user.KEYCLOAK_ID', $user['sub'])
            ->first();

            $USER_NAME = $userdetail->USER_NAME ?? 'undefined';
            $USER_GROUP_NAME = $userdetail->GROUP_NAME ?? 'undefined';
            $USER_GROUP_ID = $userdetail->USER_GROUP ?? 0;
            $USER_ID = $userdetail->USER_ID ?? 0;
            $MANAGE_DEPARTMENT_ID = $userdetail->MANAGE_DEPARTMENT_ID ?? 0;
            $MANAGE_DEPARTMENT_NAME = $userdetail->DPMT_NAME ?? 'undefined';
            $MANAGE_DIVISION_ID = $userdetail->MANAGE_DIVISION_ID ?? 0;
            $MANAGE_DIVISION_NAME = $userdetail->DIV_NAME ?? 'undefined';

            $data = array();
            $data['USER_GROUP_NAME'] = $USER_GROUP_NAME;
            $data['USER_GROUP_ID'] = $USER_GROUP_ID;
            $data['MANAGE_DEPARTMENT_NAME'] = $MANAGE_DEPARTMENT_NAME;
            $data['MANAGE_DEPARTMENT_ID'] = $MANAGE_DEPARTMENT_ID;
            $data['MANAGE_DIVISION_NAME'] = $MANAGE_DIVISION_NAME;
            $data['MANAGE_DIVISION_ID'] = $MANAGE_DIVISION_ID;

            $data['user_id'] = $USER_ID;
            $data['keycloak_id'] = $user['sub'];
            $data['name'] = $USER_NAME;
            $data['login_id'] = $user['username'];
            $data['user_type'] = 'fimm';
            $data['email'] = $user['email'];
            $data['access_token'] = $response['access_token'];
            $data['refresh_token'] = $response['refresh_token'];

            //Set session
            session(['keycloak_id' => $user['sub']]);
            session(['user_id' => $USER_ID]);
            session(['name' => $user['firstName'] . ' ' . $user['lastName']]);
            session(['USER_ID' => $USER_ID]);
            session(['USER_GROUP_NAME' => $USER_GROUP_NAME]);
            session(['GROUP_ID' => $USER_GROUP_ID]);
            session(['MANAGE_DEPARTMENT_NAME' => $MANAGE_DEPARTMENT_NAME]);
            session(['MANAGE_DEPARTMENT_ID' => $MANAGE_DEPARTMENT_ID]);
            session(['MANAGE_DIVISION_NAME' => $MANAGE_DIVISION_NAME]);
            session(['MANAGE_DIVISION_ID' => $MANAGE_DIVISION_ID]);

            http_response_code(200);
            return response([
                'message' => 'User successfully logged in.',
                'data' => $data
                // 'user_name' => $value = session('user_type')
            ]);
        }
    }

    public function logout(Request $request)
    {
        try {
            session()->forget(['keycloak_id', 'user_id', 'name']);
            session()->flush();

            http_response_code(200);
            return response([
                'message' => 'User successfully logged out.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'User failed to log out.',
                'errorCode' => 4004
            ],400);
        }
    }

    public function checkTokenValidation(){

        http_response_code(200);
        return response([
            'message' => 'Token validated.'
        ]);
    }

    public function getTokenInfo(){

        http_response_code(200);
        return response([
            'message' => json_decode(Auth::token(),true)
        ]);
    }

}
