<?php

namespace App\Http\Controllers;

use App\Models\ManageFormTemplate;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;
use File;
use Image;
use Compress\Compress;
use App\Helpers\Files;

class ManageFormTemplateController extends Controller
{
    public function get(Request $request)
    {
        try {
            $data = ManageFormTemplate::select('*')//B::table('MANAGE_FORM_TEMPLATE')
           //->select('*')//('MANAGE_FORM_TEMPLATE.MANAGE_FORM_TEMPLATE_ID','MANAGE_FORM_TEMPLATE.TEMP_TITLE','MANAGE_FORM_TEMPLATE.TEMP_DESCRIPTION','','','MANAGE_MODULE.MOD_NAME' )
           ->join('MANAGE_MODULE', 'MANAGE_MODULE.MANAGE_MODULE_ID', '=', 'MANAGE_FORM_TEMPLATE.MANAGE_MODULE_ID')
           ->where('MANAGE_FORM_TEMPLATE.MANAGE_FORM_TEMPLATE_ID',$request->MANAGE_FORM_TEMPLATE_ID)

           ->first();

            http_response_code(200);
            return response([
                'message' => 'Data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function getAll()
    {
        try {
           $data = ManageFormTemplate::select('*')
           ->select('MANAGE_FORM_TEMPLATE.MANAGE_FORM_TEMPLATE_ID','MANAGE_FORM_TEMPLATE.TEMP_TITLE','MANAGE_FORM_TEMPLATE.TEMP_DESCRIPTION','MANAGE_MODULE.MOD_NAME' )
           ->join('MANAGE_MODULE', 'MANAGE_MODULE.MANAGE_MODULE_ID', '=', 'MANAGE_FORM_TEMPLATE.MANAGE_MODULE_ID')

           ->get();

            http_response_code(200);
            return response([
                'message' => 'All data successfully retrieved.',
                'data' => $data
            ]);
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Failed to retrieve all data.', 
                'errorCode' => 4103
            ],400);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'MANAGE_MODULE_ID' => 'required|integer', //Cuti raya cina
            'TEMP_TITLE' => 'required|string', //2021-02-13
            'TEMP_DESCRIPTION' => 'required|string', //2021-02-14
            'FILEOBJECT' => 'file'
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            $destinationPath = storage_path('app/public/global/template');
            $file = $request->file('FILEOBJECT');
            $fileSize = $request->file('FILEOBJECT')->getSize();
            //save into db
            $data = new ManageFormTemplate;
            $data->MANAGE_MODULE_ID = $request->MANAGE_MODULE_ID;
            $data->TEMP_TITLE = $request->TEMP_TITLE;
            $data->TEMP_DESCRIPTION = $request->TEMP_DESCRIPTION;
            $data->TEMP_FILEPATH = $destinationPath;
            $data->TEMP_FILENAME = $request->TEMP_TITLE;
            $data->TEMP_FILESIZE = $fileSize;
            $data->TEMP_FILEEXTENSION = $file->getClientOriginalExtension();
            $data->save();

            $filenametostore = $data->MANAGE_FORM_TEMPLATE_ID . '_' . $request->TEMP_TITLE . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filenametostore);

            http_response_code(200);
            return response([
                'message' => 'Data successfully added.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be added.',
                'errorCode' => 4100
            ],400);
        }

    }

    public function getFile(Request $request)
    {
        $data = ManageFormTemplate::find($request->MANAGE_FORM_TEMPLATE_ID);
        $filePath = $data->TEMP_FILEPATH . '/' . $data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_FILENAME;
        return response()->download($filePath);
    }

    public function manage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => ''
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => '',
                'errorCode' => 4104
            ],400);
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'MANAGE_FORM_TEMPLATE_ID' => 'integer', //16
            'MANAGE_MODULE_ID' => 'integer', //1
            'TEMP_TITLE' => 'string', //consultant template
            'TEMP_DESCRIPTION' => 'string|nullable', //consultant
            'FILEOBJECT' => 'file|nullable'
        ]);
        
        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => $validator->errors()
            ],400);
        }
        try {
            $data = ManageFormTemplate::find($request->MANAGE_FORM_TEMPLATE_ID);
            
            if(Storage::disk('local')->exists('form-template/' . $data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_FILENAME)){
                if($request->file('FILEOBJECT') != null){
                    Storage::disk('local')->delete('global/template/' . $data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_FILENAME);
                    $destinationPath = storage_path('app/public/global/template');
                    $file = $request->file('FILEOBJECT');
                    $fileSize = $request->file('FILEOBJECT')->getSize();

                    $data->MANAGE_MODULE_ID = $request->MANAGE_MODULE_ID;
                    $data->TEMP_TITLE = $request->TEMP_TITLE;
                    $data->TEMP_DESCRIPTION = $request->TEMP_DESCRIPTION;
                    $data->TEMP_FILEPATH = $destinationPath;
                    $data->TEMP_FILENAME = $request->TEMP_TITLE . '.' . $file->getClientOriginalExtension();
                    $data->TEMP_FILESIZE = $fileSize;
                    $data->TEMP_FILEEXTENSION = $file->getClientOriginalExtension();
                    $data->save();

                    $filename = $file->getClientOriginalName();

                    if(!File::isDirectory(storage_path("app/public/form-template/"))){
                        Storage::makeDirectory('form-template');
                    }
                    $files = new Files();

                    $result = $files->resizeImage($file);

                    $imgs = Image::make($result)->save(storage_path("app/public/form-template/".$data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_TITLE.'.'.$file->getClientOriginalExtension()));

                    // $filenametostore = $data->MANAGE_FORM_TEMPLATE_ID . '_' . $request->TEMP_TITLE . '.' . $file->getClientOriginalExtension();
                    // $result->move($destinationPath, $filenametostore);

                    
                }else{
                    if($data->TEMP_TITLE != $request->TEMP_TITLE){
                        Storage::move('form-template/'.$data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_FILENAME, 'form-template/'.$request->MANAGE_FORM_TEMPLATE_ID . '_' . $request->TEMP_TITLE . '.' .$data->TEMP_FILEEXTENSION);
                        
                    }
                    $data->MANAGE_MODULE_ID = $request->MANAGE_MODULE_ID;
                    $data->TEMP_TITLE = $request->TEMP_TITLE;
                    $data->TEMP_DESCRIPTION = $request->TEMP_DESCRIPTION;
                    $data->TEMP_FILENAME = $request->TEMP_TITLE . '.' . $data->TEMP_FILEEXTENSION;
                    $data->save();
                    //Storage::disk('local')->delete('global/template/' . $request->MANAGE_FORM_TEMPLATE_ID . '_' . $request->TEMP_FILENAME);
                }
                

                http_response_code(200);
                return response([
                    'message' => 'Data succesfully updated'
                ]);
            }else{
                http_response_code(400);
                return response([
                    'message' => 'File not exist',
                    'errorCode' => 4107
                ],400);
            }
        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be updated.',
                'errorCode' => 4101
            ],400);
        }
    }

    public function delete(Request $request)
    {
        try {
            //**find template by id*/
            $data = ManageFormTemplate::find($request->MANAGE_FORM_TEMPLATE_ID);

            if(Storage::disk('local')->exists('form-template/' . $data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_FILENAME)){
                Storage::disk('local')->delete('form-template/' . $data->MANAGE_FORM_TEMPLATE_ID . '_' . $data->TEMP_FILENAME);
            }else{
                http_response_code(400);
                return response([
                    'message' => 'File not exist',
                    'errorCode' => 4107
                ],400);
            }

            //**delete from db */
            $data->delete();

            http_response_code(200);
            return response([
                'message' => 'Data successfully deleted.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Data failed to be deleted.',
                'errorCode' => 4102
            ],400);
        }
    }

    public function filter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'test' => 'required|string' //test
        ]);

        if ($validator->fails()) {
            http_response_code(400);
            return response([
                'message' => 'Data validation error.',
                'errorCode' => 4106
            ],400);
        }

        try {
            //manage function

            http_response_code(200);
            return response([
                'message' => 'Filtered data successfully retrieved.'
            ]);

        } catch (RequestException $r) {

            http_response_code(400);
            return response([
                'message' => 'Filtered data failed to be retrieved.',
                'errorCode' => 4105
            ],400);
        }
    }
}
