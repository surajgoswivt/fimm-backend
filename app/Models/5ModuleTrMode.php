<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class 5ModuleTrMode extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = '5MODULE_TR_MODE';

    protected $primaryKey = '5MODULE_TR_MODE_ID';

    public $timestamps = false;
}