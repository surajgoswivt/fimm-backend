<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommandController;
use App\Http\Controllers\ManageModuleController;
use App\Http\Controllers\SettingSmsController;
use App\Http\Controllers\SubModulesController;
use App\Http\Controllers\ManageSubmoduleController;
use App\Http\Controllers\ManageScreenController;
use App\Http\Controllers\SettingDashboardChartController;
use App\Http\Controllers\SettingGeneralController;
use App\Http\Controllers\ManageFormTemplateController;
use App\Http\Controllers\FiMMUserController;
use App\Http\Controllers\SettingEmailController;
use App\Http\Controllers\SettingLdapController;
use App\Http\Controllers\SettingCalendarController;
use App\Http\Controllers\DemoController;
use App\Http\Controllers\ManageEventController;
use App\Http\Controllers\ManageDivisionController;
use App\Http\Controllers\ManageRequiredDocumentController;
use App\Http\Controllers\ManageDepartmentController;
use App\Http\Controllers\ManageGroupController;
use App\Http\Controllers\ManageScreenAccessController;
use App\Http\Controllers\FinanceAccCodeController;
use App\Http\Controllers\DistributorTypeController;
use App\Http\Controllers\ConsultantTypeController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\SettingPostalController;
use App\Http\Controllers\SettingCityController;
use App\Http\Controllers\SmsTacController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ApprovalLevelController;
use App\Http\Controllers\CpdModulePointController;
use App\Http\Controllers\CPDWaiverController;
use App\Http\Controllers\CpdRenewalCalcController;
use App\Http\Controllers\CPDRuleCalcController;
use App\Http\Controllers\CpdSettingController;
use App\Http\Controllers\CpdCutOffDateController;
use App\Http\Controllers\FinanceAccGlcodeController;
use App\Http\Controllers\SettingPasswordController;
use App\Http\Controllers\DistributorApprovalController;
use App\Http\Controllers\AuthorizationLevelController;
use App\Http\Controllers\SideBarController;
use App\Http\Controllers\PendingTaskController;
use App\Http\Controllers\DistApprovalLevelController;
use App\Http\Controllers\DistributorSettingController;
use App\Http\Controllers\AnnualFeesDateController;
use App\Http\Controllers\TrModeController;
use App\Http\Controllers\FiveModuleTrModeController;
use App\Http\Controllers\ReadingTrModeController;
use App\Http\Controllers\LsAssessmentController;
use App\Http\Controllers\LsMediaController;
use App\Http\Controllers\LsMethodController;
use App\Http\Controllers\QrModeController;
use App\Http\Controllers\FpCodeController;
use App\Http\Controllers\TrProgramTypeController;
use App\Http\Controllers\ModuleCodeController;
use App\Http\Controllers\RnaVerificationPeriodController;
use App\Http\Controllers\ConsultantDesignationExemptionController;
use App\Http\Controllers\ConsultantQualificationController;
use App\Http\Controllers\ConsultantAppealController;
use App\Http\Controllers\ConsultantBankruptcyController;
use App\Http\Controllers\ConsultantAppealExaminationController;
use App\Http\Controllers\ConsultantExamSessionController;
use App\Http\Controllers\ConsultantTerminationTypeController;
use App\Http\Controllers\ConsultantTypeOfApplicationController;
use App\Http\Controllers\ConsultantActiveCeilliLicenseController;
use App\Http\Controllers\ConsultantPrsFamiliarisationController;
use App\Http\Controllers\ConsultantExaminationTypeController;
use App\Http\Controllers\ConsultantRenewalDateController;
use App\Http\Controllers\FmsFundcategoryController;
use App\Http\Controllers\FmsFundtypeController;
use App\Http\Controllers\FmsFundgroupController;
use App\Http\Controllers\FmsFundnotesController;
use App\Http\Controllers\FmsFundDomicileController;
use App\Http\Controllers\FmsSchemeStructureController;
use App\Http\Controllers\FmsRemarkOptionController;
use App\Http\Controllers\FmsReasonOptionController;
use App\Http\Controllers\FmsCutoffTimeController;
use App\Http\Controllers\DistributorFeeController;
use App\Http\Controllers\WaiverFeeController;
use App\Http\Controllers\ConsultantFeeController;
use App\Http\Controllers\AnnualFeeInvoiceController;
use App\Http\Controllers\FmsUmbrellaFundController;
use App\Http\Controllers\LoginSettingController;
use App\Http\Controllers\SystemBlockDurationController;
use App\Http\Controllers\LoginIdleSessionController;
use App\Http\Controllers\PasswordHistoryController;
use App\Http\Controllers\PasswordDefaultController;
use App\Http\Controllers\SettingUseridController;
use App\Http\Controllers\DistributorManageGroupController;
use App\Http\Controllers\DistributorManageModuleController;
use App\Http\Controllers\DistributorManageSubmoduleController;
use App\Http\Controllers\DistributorScreenAccessController;
use App\Http\Controllers\DistributorManageScreenController;
use App\Http\Controllers\ConsultantManageGroupController;
use App\Http\Controllers\ConsultantManageModuleController;
use App\Http\Controllers\ConsultantManageSubmoduleController;
use App\Http\Controllers\ConsultantManageScreenController;
use App\Http\Controllers\ThirdpartyManageGroupController;
use App\Http\Controllers\ThirdpartyManageModuleController;
use App\Http\Controllers\ThirdpartyManageSubmoduleController;
use App\Http\Controllers\ThirdPartyManageScreenController;
use App\Http\Controllers\TpManageGroupController;
use App\Http\Controllers\TpManageModuleController;
use App\Http\Controllers\TpManageSubmoduleController;
use App\Http\Controllers\TpManageScreenController;
use App\Http\Controllers\CasLetterController;
use App\Http\Controllers\ConsultantScreenAccessController;
use App\Http\Controllers\ThirdpartyScreenAccessController;
use App\Http\Controllers\TpScreenAccessController;
use App\Http\Controllers\PageMaintenanceController;
use App\Http\Controllers\CircularEventController;
use App\Http\Controllers\ManageEventApprovalController;
use App\Http\Controllers\ManageEventDocumentController;
use App\Http\Controllers\CircularEventDocumentController;
use App\Http\Controllers\CircularEventApprovalController;
use App\Http\Controllers\DistributorApprovalLevelController;

use Illuminate\Support\Facades\Route;

// Route::group(['middleware' => 'auth:api'], function () {
Route::group(['tag' => 'Demo'], function () {
    Route::post('demo-createuser', [DemoController::class, 'createuser'])->name('Create User');
    Route::post('demo-upload', [DemoController::class, 'uploadFile'])->name('Upload file/s');
    Route::post('demo-assign-group', [DemoController::class, 'assigngroup'])->name('Assign group');
    Route::get('demo-join', [DemoController::class, 'join'])->name('Join data');
    Route::get('demo-data', [DemoController::class, 'getRequest'])->name('Get data');
    Route::get('demo-read-notification', [DemoController::class, 'getNotification'])->name('Get notification');
    Route::post('demo-reset-password', [DemoController::class, 'resetPassword'])->name('Demo Reset Password');
});
// });

Route::group(['tag' => 'Authentication'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', [AuthController::class, 'logout'])->name('Logout User By Id');
        Route::get('getToken', [AuthController::class, 'getTokenInfo'])->name('Get token info');
        Route::get('checkTokenValidation', [AuthController::class, 'checkTokenValidation'])->name('Check token validation');
    });
});

Route::group(['middleware' => 'auth:api'], function () {

    Route::group(['tag' => 'Authorization Level'], function () {
        Route::get('authorization_level', [AuthorizationLevelController::class, 'get'])->name('Get authorization level');
        Route::get('authorization_levels', [AuthorizationLevelController::class, 'getAll'])->name('Get all modules');
        Route::post('authorization_level', [AuthorizationLevelController::class, 'create'])->name('Create authorization_level');
        Route::put('authorization_level', [AuthorizationLevelController::class, 'update'])->name('Update authorization_level');
        Route::delete('authorization_level', [AuthorizationLevelController::class, 'delete'])->name('Delete authorization_level');
    });

    Route::group(['tag' => 'Sidebar'], function () {
        Route::get('sidebar', [SideBarController::class, 'getSideBarByGroupId'])->name('Get sidebar by group Id');
    });

    Route::group(['tag' => 'Command'], function () {
        Route::get('data-seeder', [CommandController::class, 'seedData'])->name('Seed Data');
    });

    Route::group(['tag' => 'Pending Task'], function () {
        Route::get('pending-task', [PendingTaskController::class, 'get'])->name('Get pending task');
    });

    Route::group(['tag' => 'Chart'], function () {
        Route::post('dashboard', [SettingDashboardChartController::class, 'getChart'])->name('Generate chart');
    });

    Route::group(['tag' => 'FiMM User'], function () {
        Route::get('fimm_user/{login_id}', [FiMMUserController::class, 'get'])->name('Get FiMM User by Id');
        Route::get('fimm_users', [FiMMUserController::class, 'getAll'])->name('Get All FiMM User');
        Route::post('fimm_user', [FiMMUserController::class, 'create'])->name('Create FiMM User');
        Route::put('fimm_user', [FiMMUserController::class, 'update'])->name('Update FiMM User');
        Route::delete('fimm_user/{login_id}', [FiMMUserController::class, 'delete'])->name('Delete FiMM User by Id');
    });

    Route::group(['tag' => 'Module'], function () {
        //Route::get('module/{id}', [ManageModuleController::class, 'get'])->name('Get module by Id');
        Route::get('module', [ManageModuleController::class, 'get'])->name('Get all modules');
        Route::get('modules', [ManageModuleController::class, 'getAll'])->name('Get all modules');
        Route::post('module', [ManageModuleController::class, 'create'])->name('Create module');
        Route::put('module', [ManageModuleController::class, 'update'])->name('Update module');
        Route::delete('module', [ManageModuleController::class, 'delete'])->name('Delete module');
    });

    Route::group(['tag' => 'Submodule'], function () {
        Route::post('submodule', [ManageSubmoduleController::class, 'create'])->name('Create submodule from module Id');
        Route::get('submodule', [ManageSubmoduleController::class, 'get'])->name('Get submodule by Id');
        Route::get('submoduless', [ManageSubmoduleController::class, 'getSubmodule'])->name('Get submodule by Id');
        Route::get('submodules', [ManageSubmoduleController::class, 'getAll'])->name('Get all submodules');
        // Route::delete('submodule/{moduleId}', [ManageSubmoduleController::class, 'delete'])->name('Delete submodule by module Id');
        Route::delete('submodule', [ManageSubmoduleController::class, 'delete'])->name('Delete submodule by module Id');
        Route::put('submodule', [ManageSubmoduleController::class, 'update'])->name('Update submodule');
    });

    Route::group(['tag' => 'Screen'], function () {
        Route::post('screen', [ManageScreenController::class, 'create'])->name('Create screen');
        Route::get('screen', [ManageScreenController::class, 'get'])->name('Get screen by module/submodule Id');
        Route::get('screen_byId', [ManageScreenController::class, 'getScreenId'])->name('Get screen by module/submodule Id');
        Route::get('screen_processFlow', [ManageScreenController::class, 'getProcessFlow'])->name('Get screen by module/submodule Id');
        Route::get('screen_submodule', [ManageScreenController::class, 'getSubmodule'])->name('Get screen by module/submodule Id');
        Route::get('screens', [ManageScreenController::class, 'getAll'])->name('Get all submodules');
        Route::delete('screen', [ManageScreenController::class, 'delete'])->name('Delete submodule by module Id');
        Route::put('screen', [ManageScreenController::class, 'update'])->name('Update submodule');
    });

    Route::group(['tag' => 'Screen Access'], function () {
        Route::get('screen_access', [ManageScreenAccessController::class, 'get'])->name('Get screen access by Id');
        Route::get('screen_access_user', [ManageScreenAccessController::class, 'getUser'])->name('Get screen access by Id');
        Route::get('screen_accesses', [ManageScreenAccessController::class, 'getAll'])->name('Get all screen access');
        Route::post('screen_access', [ManageScreenAccessController::class, 'create'])->name('Create screen access');
        Route::put('screen_access', [ManageScreenAccessController::class, 'update'])->name('Update screen Access');
        Route::get('screen_access_auth', [ManageScreenAccessController::class, 'getAuthorization'])->name('Get screen access by Id');
        Route::get('screen_access_group', [ManageScreenAccessController::class, 'getAllGroup'])->name('Get screen access by Id');
    });

    Route::group(['tag' => 'Notification'], function () {
        Route::get('notifications', [NotificationController::class, 'get'])->name('Get notifications');
        Route::post('update_notifications', [NotificationController::class, 'update'])->name('Get Distributor notifications');
        Route::get('distributor_notifications', [NotificationController::class, 'getDistributor'])->name('Get Distributor notifications');
        Route::post('update_distributor_notifications', [NotificationController::class, 'updateDistributor'])->name('Get Distributor notifications');
        Route::get('consultant_notifications', [NotificationController::class, 'getDistributor'])->name('Get Distributor notifications');
        Route::post('update_consultant_notifications', [NotificationController::class, 'updateDistributor'])->name('Get Distributor notifications');
        Route::get('others_notifications', [NotificationController::class, 'getDistributor'])->name('Get Distributor notifications');
        Route::post('update_others_notifications', [NotificationController::class, 'updateDistributor'])->name('Get Distributor notifications');
    });

    Route::group(['tag' => 'Template'], function () {
        Route::get('template', [ManageFormTemplateController::class, 'get'])->name('Get template by Id');
        Route::get('templates', [ManageFormTemplateController::class, 'getAll'])->name('Get all templates');
        Route::post('template', [ManageFormTemplateController::class, 'create'])->name('Create template');
        Route::get('template_file', [ManageFormTemplateController::class, 'getFile'])->name('Get template file by template Id');
        Route::delete('template_file', [ManageFormTemplateController::class, 'delete'])->name('Delete template file by template file Id');
        Route::put('template_file', [ManageFormTemplateController::class, 'update'])->name('Update template');
    });

    Route::group(['tag' => 'SMS Integration'], function () {
        Route::get('sms_setting', [SettingSmsController::class, 'getAll'])->name('Get SMS setting');
        Route::get('sms_record', [SettingSmsController::class, 'getLog'])->name('Get SMS log record');
        Route::post('sms_setting', [SettingSmsController::class, 'manage'])->name('Manage SMS setting');
        Route::post('sms_setting', [SettingSmsController::class, 'create'])->name('Manage SMS setting');
        Route::post('sms_testing', [SettingSmsController::class, 'test'])->name('SMS testing');
        Route::post('sms', [SettingSmsController::class, 'send'])->name('Send SMS');
        Route::post('sms_TAC', [SmsTacController::class, 'getTAC'])->name('Get TAC');
        Route::get('sms_verify_TAC', [SmsTacController::class, 'verifyTAC'])->name('Verify TAC');
    });

    Route::group(['tag' => 'Email Integration'], function () {
        Route::get('email_setting', [SettingEmailController::class, 'get'])->name('Get email setting');
        Route::post('email_setting', [SettingEmailController::class, 'manage'])->name('Manage email setting');
        Route::post('email', [SettingEmailController::class, 'send'])->name('Send Email');
    });

    Route::group(['tag' => 'LDAP Integration'], function () {
        Route::get('ldap_setting', [SettingLdapController::class, 'get'])->name('Get LDAP setting');
        Route::post('ldap_setting', [SettingLdapController::class, 'manage'])->name('Manage LDAP setting');
        Route::post('ldap_testing', [SettingLdapController::class, 'test'])->name('LDAP testing');
        Route::post('ldap_sync', [SettingLdapController::class, 'sync'])->name('Sync Users from Active Directory');
    });

    Route::group(['tag' => 'Calendar Setting'], function () {
        Route::post('calendar_setting', [SettingCalendarController::class, 'create'])->name('Create Calendar setting');
        Route::get('calendar_settings', [SettingCalendarController::class, 'getAll'])->name('Get all exception');
        Route::get('calendar_setting', [SettingCalendarController::class, 'get'])->name('Get Exception by Id');
        Route::delete('calendar_setting', [SettingCalendarController::class, 'delete'])->name('Delete exception');
        Route::put('calendar_setting', [SettingCalendarController::class, 'update'])->name('Update exception');
        Route::get('filter_calendar_setting', [SettingCalendarController::class, 'filter'])->name('Search exception');
    });

    Route::group(['tag' => 'Event Management'], function () {
        Route::post('event_management', [ManageEventController::class, 'create'])->name('Create Event');
        Route::post('event_management/update', [ManageEventController::class, 'update'])->name('Update Event');
        Route::get('event_managements', [ManageEventController::class, 'getAll'])->name('Get all event');
        Route::get('event_management', [ManageEventController::class, 'get'])->name('Get event by Id');
        Route::post('event_management/delete', [ManageEventController::class, 'delete'])->name('Delete');
    });
    Route::group(['tag' => 'Event Management Document'], function () {
        Route::get('event_document', [ManageEventDocumentController::class, 'get'])->name('Get all event');
    });
    Route::group(['tag' => 'Event Management Approval'], function () {
        Route::post('event_approval', [ManageEventApprovalController::class, 'create'])->name('Create Event');
        Route::put('event_approval', [ManageEventApprovalController::class, 'update'])->name('Update Event');
        Route::get('event_approvals', [ManageEventApprovalController::class, 'getAll'])->name('Get all event');
        Route::get('event_approval', [ManageEventApprovalController::class, 'get'])->name('Get event by Id');
        Route::get('event_approvalss', [ManageEventApprovalController::class, 'getApprList'])->name('Get event by Id');
        Route::post('event_approval', [ManageEventApprovalController::class, 'delete'])->name('Delete');
    });
    Route::group(['tag' => 'Circular Management'], function () {
        Route::post('create_circular_management', [CircularEventController::class, 'createNewCircular'])->name('Create New Circular');
        Route::put('circular_management', [CircularEventController::class, 'update'])->name('Update circular');
        Route::get('circular_managements', [CircularEventController::class, 'getAll'])->name('Get all circular');
        Route::get('circular_management', [CircularEventController::class, 'get'])->name('Get event by Id');
        Route::get('circular_management', [CircularEventController::class, 'delete'])->name('Delete');
    });
    Route::group(['tag' => 'Circular Document'], function () {
        Route::get('circular_document', [CircularEventDocumentController::class, 'get'])->name('Get all event');
    });
    Route::group(['tag' => 'Circular Approval'], function () {
        Route::get('circular_approval', [CircularEventApprovalController::class, 'CirculargetApprList'])->name('Get all event');
    });
    Route::group(['tag' => 'Division Management'], function () {
        Route::post('division_management', [ManageDivisionController::class, 'create'])->name('Create Division');
        Route::get('division_managements', [ManageDivisionController::class, 'getAll'])->name('Get all division');
        Route::get('division_management', [ManageDivisionController::class, 'get'])->name('Get division by Id');
        Route::delete('division_management', [ManageDivisionController::class, 'delete'])->name('Delete division');
        Route::put('division_management', [ManageDivisionController::class, 'update'])->name('Update division');
    });
    Route::group(['tag' => 'Department Management'], function () {
        Route::post('department_management', [ManageDepartmentController::class, 'create'])->name('Create department');
        Route::get('department_managements', [ManageDepartmentController::class, 'getAll'])->name('Get all department');
        Route::get('department_management', [ManageDepartmentController::class, 'get'])->name('Get department by Id');
        Route::get('department_management_by_division', [ManageDepartmentController::class, 'getByDivision'])->name('Get department by division Id');
        Route::delete('department_management', [ManageDepartmentController::class, 'delete'])->name('Delete department');
        Route::put('department_management', [ManageDepartmentController::class, 'update'])->name('Update department');
        Route::get('filter_department_management', [ManageDepartmentController::class, 'filter'])->name('Filter department');
    });
    Route::group(['tag' => 'Group Management'], function () {
        Route::post('group_management', [ManageGroupController::class, 'create'])->name('Create group');
        Route::get('group_managements', [ManageGroupController::class, 'getAll'])->name('Get all group');
        Route::get('group_management', [ManageGroupController::class, 'get'])->name('Get group by Id');
        Route::delete('group_management', [ManageGroupController::class, 'delete'])->name('Delete group');
        Route::get('group_management_by_department', [ManageGroupController::class, 'getByDepartment'])->name('Get group by department Id');
        Route::put('group_management', [ManageGroupController::class, 'update'])->name('Update group');
    });
    Route::group(['tag' => 'Required Document'], function () {
        Route::post('required_document', [ManageRequiredDocumentController::class, 'create'])->name('Create required document');
        Route::get('sub_modules', [ManageRequiredDocumentController::class, 'getSubModule'])->name('Get sub module');
        Route::get('doc_type', [ManageRequiredDocumentController::class, 'getDocType'])->name('Get document type');
        Route::get('required_document', [ManageRequiredDocumentController::class, 'get'])->name('Get required document');
        Route::get('filter_required_document', [ManageRequiredDocumentController::class, 'filter'])->name('Filter required document');
        Route::delete('required_document', [ManageRequiredDocumentController::class, 'delete'])->name('Delete required document');
        Route::get('required_document_byId', [ManageRequiredDocumentController::class, 'getById'])->name('Get required document by id');
        Route::put('required_document', [ManageRequiredDocumentController::class, 'update'])->name('Update required document');
        Route::get('required_document_proposal', [ManageRequiredDocumentController::class, 'getDocumentProposal'])->name('get required document proposal');
        Route::get('required_document_additional', [ManageRequiredDocumentController::class, 'getDocumentAdditional'])->name('get required document proposal');
    });
    Route::group(['tag' => 'Finance Account Code'], function () {
        Route::post('finance_account_code', [FinanceAccCodeController::class, 'create'])->name('Create Account');
        Route::get('finance_account_code', [FinanceAccCodeController::class, 'getAll'])->name('Get all Account');
        Route::get('finance_account_code', [FinanceAccCodeController::class, 'get'])->name('Get Account by Id');
        // Route::get('finance_account_code', [FinanceAccCodeController::class, 'get'])->name('Get Account by Id');
        Route::get('finance_account_type', [FinanceAccCodeController::class, 'getAccCodeType'])->name('Get Account by Id');
        Route::get('finance_account_name', [FinanceAccCodeController::class, 'getAccCodeName'])->name('Get Account by Id');
        Route::delete('finance_account_code', [FinanceAccCodeController::class, 'delete'])->name('Delete Account Code');
    });
    Route::group(['tag' => 'Finance Setting Code'], function () {
        Route::post('finance_setting_code', [FinanceAccGlcodeController::class, 'create'])->name('Create Setting Code');
        Route::get('finance_setting_codes', [FinanceAccGlcodeController::class, 'getAll'])->name('Get all Setting Code');
        Route::get('finance_setting_code', [FinanceAccGlcodeController::class, 'get'])->name('Get Setting Code by Id');
        Route::delete('finance_setting_code', [FinanceAccGlcodeController::class, 'delete'])->name('Delete Setting Code');
        Route::get('distributor_list', [FinanceAccGlcodeController::class, 'getAllDistributor'])->name('Get all Setting Code');
        Route::get('fin_code_table', [FinanceAccGlcodeController::class, 'getCodeTable'])->name('Get Setting Code by Id');
    });
    Route::group(['tag' => 'Setting General'], function () {
        Route::post('setting_general', [SettingGeneralController::class, 'create'])->name('Create setting');
        Route::get('setting_generals', [SettingGeneralController::class, 'getAll'])->name('Get all setting');
        Route::get('setting_general', [SettingGeneralController::class, 'get'])->name('Get setting by Id');
        Route::delete('setting_general', [SettingGeneralController::class, 'delete'])->name('Delete setting');
        Route::put('setting_general', [SettingGeneralController::class, 'update'])->name('Update setting');
        Route::get('other_type', [SettingGeneralController::class, 'getOtherUserCategory'])->name('Get other user category');
        Route::get('get_state', [SettingGeneralController::class, 'getAllState'])->name('Get All State');
        Route::get('get_states', [SettingGeneralController::class, 'getState'])->name('Get State by ID');
        Route::get('get_country', [SettingGeneralController::class, 'getAllCountry'])->name('Get All State');
        Route::get('get_countrys', [SettingGeneralController::class, 'getCountry'])->name('Get State by ID');
        Route::get('filter_setting_general', [SettingGeneralController::class, 'filter'])->name('Search exception');
        Route::get('filter_country', [SettingGeneralController::class, 'filterCountry'])->name('Search exception');
        Route::get('currency_format', [SettingGeneralController::class, 'getCurrency'])->name('get currency format');
        Route::get('getAllCurrencyFormat', [SettingGeneralController::class, 'getAllCurrency'])->name('get allcurrency format');
        Route::post('bulk_upload', [SettingGeneralController::class, 'bulkUpload'])->name('create bulk upload');

        // Route::post('bulk_countrylist', [SettingGeneralController::class, 'getCountryList'])->name('create bulk upload');
    });
    Route::group(['tag' => 'Distributor Setting'], function () {
        Route::post('dist_setting', [DistributorSettingController::class, 'create'])->name('Create distributor setting');
        Route::get('dist_settings', [DistributorSettingController::class, 'getAll'])->name('Get all distributor setting');
        Route::get('dist_setting', [DistributorSettingController::class, 'get'])->name('Get distributor setting by Id');
        Route::delete('dist_setting', [DistributorSettingController::class, 'delete'])->name('Delete distributor setting');
        Route::put('dist_setting', [DistributorSettingController::class, 'update'])->name('Update distributor setting');
        Route::get('dist_declare', [DistributorSettingController::class, 'getAllDeclaration'])->name('Get all distributor Declaration');
    });
    Route::group(['tag' => 'Setting Postcode'], function () {
        Route::post('setting_postcode', [SettingPostalController::class, 'create'])->name('Create Postcode');
        Route::get('setting_postcodes', [SettingPostalController::class, 'getAll'])->name('Get all Postcode');
        Route::get('setting_postcode', [SettingPostalController::class, 'get'])->name('Get Postcode by Id');
        // Route::get('setting_postcode', [SettingPostalController::class, 'get'])->name('Get Postcode by Id');
        Route::put('setting_postcode', [SettingPostalController::class, 'update'])->name('Update setting Postcode');
        Route::get('filter_setting_postcode', [SettingPostalController::class, 'filter'])->name('Filter');
        Route::delete('setting_postcode', [SettingPostalController::class, 'delete'])->name('Delete setting');
    });
    Route::group(['tag' => 'Setting City'], function () {
        Route::post('setting_city', [SettingCityController::class, 'create'])->name('Create Postcode');
        //Route::get('setting_city', [SettingCityController::class, 'getAll'])->name('Get all Postcode');
        // Route::get('setting_city', [SettingCityController::class, 'get'])->name('Get Postcode by Id');
        Route::put('setting_city', [SettingCityController::class, 'update'])->name('Update setting Postcode');
    });
    Route::group(['tag' => 'Approval Level'], function () {
        Route::post('approval_level', [ApprovalLevelController::class, 'create'])->name('Create Approval Level');
        Route::get('approval_levels', [ApprovalLevelController::class, 'getAll'])->name('Get all Approval Level');
        Route::get('approval_levelss', [ApprovalLevelController::class, 'getAllbyName'])->name('getAllbyName Approval Level');
        Route::get('approval_level', [ApprovalLevelController::class, 'get'])->name('Get Approval Level by Id');
        Route::get('approval_level_byindex', [ApprovalLevelController::class, 'getByIndex'])->name('Get Approval Level by index');
        Route::get('get_department', [ApprovalLevelController::class, 'getAllDepartment'])->name('Get all Department list');
        Route::get('get_distributor', [ApprovalLevelController::class, 'getAllDistibutorType'])->name('Get all Department list');
        Route::put('approval_level', [ApprovalLevelController::class, 'update'])->name('Update Approval Level');
    });
    Route::group(['tag' => 'Distributor Approval Level'], function () {
        Route::post('dist_approval', [DistApprovalLevelController::class, 'create'])->name('Create Postcode');
        Route::get('get_group', [DistApprovalLevelController::class, 'getAllGroup'])->name('Get all Group');
        Route::get('dist_approvals', [DistApprovalLevelController::class, 'getAll'])->name('Get all');
        Route::get('dist_approval', [DistApprovalLevelController::class, 'get'])->name('Get Postcode by Id');
        Route::put('dist_approval', [DistApprovalLevelController::class, 'update'])->name('Update setting Postcode');
    });
    Route::group(['tag' => 'CPD Module Point'], function () {
        Route::get('cpd_point', [CpdModulePointController::class, 'get'])->name('Get Cpd Point');
        Route::get('cpd_points', [CpdModulePointController::class, 'getAll'])->name('Get All Cpd Point');
        Route::get('five_modules', [CpdModulePointController::class, 'getAllfiveModule'])->name('Get All Cpd Point');
        Route::post('cpd_point', [CpdModulePointController::class, 'create'])->name('create Cpd Point');
        Route::put('cpd_point', [CpdModulePointController::class, 'update'])->name('create Cpd Point');
        Route::delete('cpd_point', [CpdModulePointController::class, 'delete'])->name('create Cpd Point'); //

    });
    Route::group(['tag' => 'CPD Waiver'], function () {
        Route::get('cpd_waiver', [CPDWaiverController::class, 'get'])->name('Get CPD Waiver');
        Route::get('cpd_waivers', [CPDWaiverController::class, 'getAll'])->name('Get All CPD Waiver');
        Route::post('cpd_waiver', [CPDWaiverController::class, 'create'])->name('create CPD Waiver');
        Route::put('cpd_waiver', [CPDWaiverController::class, 'update'])->name('Update CPD Waiver');
        Route::delete('cpd_waiver', [CPDWaiverController::class, 'delete'])->name('create CPD Waiver');
    });
    Route::group(['tag' => 'CPD Renewal Calculation'], function () {
        Route::get('cpd_renewal', [CpdRenewalCalcController::class, 'get'])->name('Get cpd renewal');
        Route::get('cpd_renewals', [CpdRenewalCalcController::class, 'getAll'])->name('Get All cpd renewal');
        Route::post('cpd_renewal', [CpdRenewalCalcController::class, 'create'])->name('create cpd renewal');
        Route::put('cpd_renewal', [CpdRenewalCalcController::class, 'update'])->name('create cpd renewal');
        Route::delete('cpd_renewal', [CpdRenewalCalcController::class, 'delete'])->name('create cpd renewal');
    });
    Route::group(['tag' => 'CPD Rule Renewal Calculation'], function () {
        Route::get('cpd_rule', [CPDRuleCalcController::class, 'get'])->name('Get cpd rule');
        Route::get('cpd_rules', [CPDRuleCalcController::class, 'getAll'])->name('Get All cpd rule');
        Route::post('cpd_rule', [CPDRuleCalcController::class, 'create'])->name('create cpd rule');
        Route::put('cpd_rule', [CPDRuleCalcController::class, 'update'])->name('create cpd rule');
        Route::delete('cpd_rule', [CPDRuleCalcController::class, 'delete'])->name('create cpd rule');
    });
    Route::group(['tag' => 'CPD Setting'], function () {
        Route::get('cpd_setting', [CpdSettingController::class, 'get'])->name('Get cpd setting');
        Route::get('cpd_settings', [CpdSettingController::class, 'getAll'])->name('Get All cpd setting');
        Route::post('cpd_setting', [CpdSettingController::class, 'create'])->name('create cpd setting');
        Route::put('cpd_setting', [CpdSettingController::class, 'update'])->name('create cpd setting');
        Route::delete('cpd_setting', [CpdSettingController::class, 'delete'])->name('create cpd setting');
    });
    Route::group(['tag' => 'CPD Cut Off Date'], function () {
        Route::get('cpd_dates', [CpdCutOffDateController::class, 'get'])->name('Get cpd cut of date');
        Route::get('cpd_date', [CpdCutOffDateController::class, 'getAll'])->name('Get All cpd cut of date');
        Route::post('cpd_date', [CpdCutOffDateController::class, 'create'])->name('create cpd cut of date');
        Route::put('cpd_date', [CpdCutOffDateController::class, 'update'])->name('create cpd cut of date');
        Route::delete('cpd_date', [CpdCutOffDateController::class, 'delete'])->name('create cpd cut of date');
    });
    Route::group(['tag' => 'Annual Fee Date'], function () {
        Route::post('annual_date', [AnnualFeesDateController::class, 'create'])->name('Create Annual fee Date');
        Route::get('annual_date', [AnnualFeesDateController::class, 'getAll'])->name('Get all Annual fee Date');
        Route::get('annual_dates', [AnnualFeesDateController::class, 'get'])->name('Get Annual fee Date by Id');
        Route::get('annual_date_by_id', [AnnualFeesDateController::class, 'getbyId'])->name('Get Annual fee Date by Id');
        Route::get('annual_datess', [AnnualFeesDateController::class, 'getAllListDate'])->name('Get all Annual fee Date');
        Route::put('annual_date', [AnnualFeesDateController::class, 'update'])->name('create Annual fee Date');
    });
    Route::group(['tag' => 'R&A VERIFICATION DATE'], function () {
        Route::post('rna_date', [RnaVerificationPeriodController::class, 'create'])->name('Create  Date');
        Route::get('rna_dates', [RnaVerificationPeriodController::class, 'getAll'])->name('Get all Date');
        // Route::get('annual_dates', [AnnualFeesDateController::class, 'get'])->name('Get Annual fee Date by Id');
        // Route::get('annual_datess', [AnnualFeesDateController::class, 'getAllListDate'])->name('Get all Annual fee Date');
    });

    Route::group(['tag' => 'Distributor Type'], function () {
        Route::get('distributor_types', [DistributorTypeController::class, 'getAll'])->name('Get all distributor type');
        Route::get('distributor_type', [DistributorTypeController::class, 'get'])->name('Get all distributor type');
        Route::get('distributor_typess', [DistributorTypeController::class, 'getDistType'])->name('Get all distributor type');
        Route::post('distributor_type', [DistributorTypeController::class, 'create'])->name('create dist Type');
        Route::put('distributor_type', [DistributorTypeController::class, 'update'])->name('Update Dist Type');
        Route::delete('distributor_type', [DistributorTypeController::class, 'delete'])->name('Delete Dist Type');
        Route::get('dist_marketing', [DistributorTypeController::class, 'getMarketing'])->name('Get all marketing list');
        Route::get('consultant_type', [DistributorTypeController::class, 'getConsultantType'])->name('Get all Consultant type');
    });
    Route::group(['tag' => 'TR Mode Pre and Post vetting'], function () {
        Route::get('tr_modess', [TrModeController::class, 'getAll'])->name('Get all TR Mode Pre and Post vetting');
        Route::get('tr_mode', [TrModeController::class, 'get'])->name('Get TR Mode Pre and Post vetting');
        Route::post('tr_mode', [TrModeController::class, 'create'])->name('create TR Mode Pre and Post vetting');
        Route::put('tr_mode', [TrModeController::class, 'update'])->name('create TR Mode Pre and Post vetting');
        Route::delete('tr_mode', [TrModeController::class, 'delete'])->name('create TR Mode Pre and Post vetting');
    });
    Route::group(['tag' => 'TR Mode 5 Module'], function () {
        Route::get('five_trs', [FiveModuleTrModeController::class, 'getAll'])->name('Get all TR 5 Module');
        Route::get('five_tr', [FiveModuleTrModeController::class, 'get'])->name('Get TR Mode 5 Module');
        Route::post('five_tr', [FiveModuleTrModeController::class, 'create'])->name('create TR Mode 5 Module');
        Route::put('five_tr', [FiveModuleTrModeController::class, 'update'])->name('create TR Mode 5 Module');
        Route::delete('five_tr', [FiveModuleTrModeController::class, 'delete'])->name('create TR Mode 5 Module');
    });
    Route::group(['tag' => 'TR Mode Reading'], function () {
        Route::get('reading_trss', [ReadingTrModeController::class, 'getAll'])->name('Get all TR Reading');
        Route::get('reading_tr', [ReadingTrModeController::class, 'get'])->name('Get TR Mode Reading');
        Route::post('reading_tr', [ReadingTrModeController::class, 'create'])->name('create TR Mode Reading');
        Route::put('reading_tr', [ReadingTrModeController::class, 'update'])->name('create TR Mode Reading');
        Route::delete('reading_tr', [ReadingTrModeController::class, 'delete'])->name('create TR Mode Reading');
    });
    Route::group(['tag' => 'LS ASSESSMENT'], function () {
        Route::get('ls_assessments', [LsAssessmentController::class, 'getAll'])->name('Get all LS ASSESSMENT');
        Route::get('ls_assessment', [LsAssessmentController::class, 'get'])->name('Get LS ASSESSMENT');
        Route::post('ls_assessment', [LsAssessmentController::class, 'create'])->name('create LS ASSESSMENT');
        Route::put('ls_assessment', [LsAssessmentController::class, 'update'])->name('update LS ASSESSMENT');
        Route::delete('ls_assessment', [LsAssessmentController::class, 'delete'])->name('create LS ASSESSMENT');
    });
    Route::group(['tag' => 'LS MEDIA'], function () {
        Route::get('ls_medias', [LsMediaController::class, 'getAll'])->name('Get all LS MEDIA');
        Route::get('ls_media', [LsMediaController::class, 'get'])->name('Get LS MEDIA');
        Route::post('ls_media', [LsMediaController::class, 'create'])->name('create LS MEDIA');
        Route::put('ls_media', [LsMediaController::class, 'update'])->name('update LS MEDIA');
        Route::delete('ls_media', [LsMediaController::class, 'delete'])->name('create LS MEDIA');
    });
    Route::group(['tag' => 'LS METHOD'], function () {
        Route::get('ls_methods', [LsMethodController::class, 'getAll'])->name('Get all LS METHOD');
        Route::get('ls_method', [LsMethodController::class, 'get'])->name('Get LS METHOD');
        Route::post('ls_method', [LsMethodController::class, 'create'])->name('create LS METHOD');
        Route::put('ls_method', [LsMethodController::class, 'update'])->name('update LS METHOD');
        Route::delete('ls_method', [LsMethodController::class, 'delete'])->name('create LS METHOD');
    });
    Route::group(['tag' => 'QR MODE'], function () {
        Route::get('qr_modes', [QrModeController::class, 'getAll'])->name('Get all QR MODE');
        Route::get('qr_mode', [QrModeController::class, 'get'])->name('Get QR MODE');
        Route::post('qr_mode', [QrModeController::class, 'create'])->name('create QR MODE');
        Route::put('qr_mode', [QrModeController::class, 'update'])->name('update QR MODE');
        Route::delete('qr_mode', [QrModeController::class, 'delete'])->name('create QR MODE');
    });
    Route::group(['tag' => 'FP CODE'], function () {
        Route::get('fp_codes', [FpCodeController::class, 'getAll'])->name('Get all FP CODE');
        Route::get('fp_code', [FpCodeController::class, 'get'])->name('Get FP CODE');
        Route::post('fp_code', [FpCodeController::class, 'create'])->name('create FP CODE');
        Route::put('fp_code', [FpCodeController::class, 'update'])->name('update FP CODE');
        Route::delete('fp_code', [FpCodeController::class, 'delete'])->name('create FP CODE');
    });
    Route::group(['tag' => 'MODULE CODE'], function () {
        Route::get('module_codes', [ModuleCodeController::class, 'getAll'])->name('Get all MODULE CODE');
        Route::get('module_code', [ModuleCodeController::class, 'get'])->name('Get MODULE CODE');
        Route::post('module_code', [ModuleCodeController::class, 'create'])->name('create MODULE CODE');
        Route::put('module_code', [ModuleCodeController::class, 'update'])->name('update MODULE CODE');
        Route::delete('module_code', [ModuleCodeController::class, 'delete'])->name('create MODULE CODE');
    });
    Route::group(['tag' => 'TR PROGRAM TYPE'], function () {
        Route::get('tr_programs', [TrProgramTypeController::class, 'getAll'])->name('Get all TR PROGRAM TYPE');
        Route::get('tr_program', [TrProgramTypeController::class, 'get'])->name('Get TR PROGRAM TYPE');
        Route::post('tr_program', [TrProgramTypeController::class, 'create'])->name('create TR PROGRAM TYPE');
        Route::put('tr_program', [TrProgramTypeController::class, 'update'])->name('update TR PROGRAM TYPE');
        Route::delete('tr_program', [TrProgramTypeController::class, 'delete'])->name('create TR PROGRAM TYPE');
    });
    Route::group(['tag' => 'Consultant Type'], function () {
        Route::get('cons_types', [ConsultantTypeController::class, 'getAll'])->name('Get all consultant type');
        Route::get('cons_type', [ConsultantTypeController::class, 'get'])->name('Get Consultant Type');
        Route::post('cons_type', [ConsultantTypeController::class, 'create'])->name('create Consultant Type');
        Route::put('cons_type', [ConsultantTypeController::class, 'update'])->name('update Consultant Type');
        Route::delete('cons_type', [ConsultantTypeController::class, 'delete'])->name('create Consultant Type');
    });
    Route::group(['tag' => 'Address management'], function () {
        Route::get('setting_postalByID', [SettingPostalController::class, 'getPostalByID'])->name('Get Postal By ID');
        Route::get('setting_postal', [SettingPostalController::class, 'getAll'])->name('Get all postal');
        Route::get('setting_city', [SettingCityController::class, 'get'])->name('Get all city by id');
    });
    Route::group(['tag' => 'Consultant Qualification'], function () {
        Route::get('cons_quals', [ConsultantQualificationController::class, 'getAll'])->name('Get all consultant Qualification');
        Route::get('cons_qual', [ConsultantQualificationController::class, 'get'])->name('Get Consultant Qualification');
        Route::post('cons_qual', [ConsultantQualificationController::class, 'create'])->name('create Consultant Qualification');
        Route::put('cons_qual', [ConsultantQualificationController::class, 'update'])->name('update Consultant Qualification');
        Route::delete('cons_qual', [ConsultantQualificationController::class, 'delete'])->name('delete Consultant Qualification');
    });
    Route::group(['tag' => 'Consultant Designation Exemption'], function () {
        Route::get('cons_designs', [ConsultantDesignationExemptionController::class, 'getAll'])->name('Get all consultant Designation Exemption');
        Route::get('cons_design', [ConsultantDesignationExemptionController::class, 'get'])->name('Get Consultant Designation Exemption');
        Route::post('cons_design', [ConsultantDesignationExemptionController::class, 'create'])->name('create Consultant Designation Exemption');
        Route::put('cons_design', [ConsultantDesignationExemptionController::class, 'update'])->name('update Consultant Designation Exemption');
        Route::delete('cons_design', [ConsultantDesignationExemptionController::class, 'delete'])->name('create Consultant Designation Exemption');
    });
    Route::group(['tag' => 'Consultant Appeal'], function () {
        Route::get('cons_appeal', [ConsultantAppealController::class, 'get'])->name('Get Consultant Appeal');
        Route::post('cons_appeal', [ConsultantAppealController::class, 'create'])->name('create Consultant Appeal');
    });
    Route::group(['tag' => 'Consultant Appeal Exam'], function () {
        Route::get('cons_appeal_exam', [ConsultantAppealExaminationController::class, 'get'])->name('Get Consultant Appeal Exam');
        Route::post('cons_appeal_exam', [ConsultantAppealExaminationController::class, 'create'])->name('create Consultant Appeal Exam');
    });
    Route::group(['tag' => 'Consultant Bankruptcy Check'], function () {
        Route::get('cons_bankruptcy', [ConsultantBankruptcyController::class, 'get'])->name('Get Bankruptcy Check');
        Route::post('cons_bankruptcy', [ConsultantBankruptcyController::class, 'create'])->name('create');
    });
    Route::group(['tag' => 'Consultant PRS Familiarisation'], function () {
        Route::get('cons_prs', [ConsultantPrsFamiliarisationController::class, 'get'])->name('Get Active Ceilli');
        Route::post('cons_prs', [ConsultantPrsFamiliarisationController::class, 'create'])->name('create Active Ceilli');
    });
    Route::group(['tag' => 'Consultant Active Ceilli'], function () {
        Route::get('cons_ceilli', [ConsultantActiveCeilliLicenseController::class, 'get'])->name('Get PRS Familiarisation');
        Route::post('cons_ceilli', [ConsultantActiveCeilliLicenseController::class, 'create'])->name('create PRS Familiarisation');
    });
    Route::group(['tag' => 'Consultant Exam Type'], function () {
        Route::get('cons_exam_types', [ConsultantExaminationTypeController::class, 'getAll'])->name('Get all consultant  Exam Type');
        Route::get('cons_exam_type', [ConsultantExaminationTypeController::class, 'get'])->name('Get Consultant  Exam Type');
        Route::post('cons_exam_type', [ConsultantExaminationTypeController::class, 'create'])->name('create Consultant  Exam Type');
        Route::put('cons_exam_type', [ConsultantExaminationTypeController::class, 'update'])->name('update Consultant  Exam Type');
        Route::delete('cons_exam_type', [ConsultantExaminationTypeController::class, 'delete'])->name('create Consultant  Exam Type');
    });
    Route::group(['tag' => 'Consultant Exam Session'], function () {
        Route::get('cons_exam_sessions', [ConsultantExamSessionController::class, 'getAll'])->name('Get all consultant   Exam Session');
        Route::get('cons_exam_session', [ConsultantExamSessionController::class, 'get'])->name('Get Consultant   Exam Session');
        Route::post('cons_exam_session', [ConsultantExamSessionController::class, 'create'])->name('create Consultant   Exam Session');
        Route::put('cons_exam_session', [ConsultantExamSessionController::class, 'update'])->name('update Consultant   Exam Session');
        Route::delete('cons_exam_session', [ConsultantExamSessionController::class, 'delete'])->name('create Consultant   Exam Session');
    });
    Route::group(['tag' => 'Consultant Type Of Application'], function () {
        Route::get('cons_type_appls', [ConsultantTypeOfApplicationController::class, 'getAll'])->name('Get all consultant   Type Of Application');
        Route::get('cons_type_appl', [ConsultantTypeOfApplicationController::class, 'get'])->name('Get Consultant   Type Of Application');
        Route::post('cons_type_appl', [ConsultantTypeOfApplicationController::class, 'create'])->name('create Consultant   Type Of Application');
        Route::put('cons_type_appl', [ConsultantTypeOfApplicationController::class, 'update'])->name('update Consultant   Type Of Application');
        Route::delete('cons_type_appl', [ConsultantTypeOfApplicationController::class, 'delete'])->name('create Consultant   Type Of Application');
    });
    Route::group(['tag' => 'Consultant Termination'], function () {
        Route::get('cons_terminates', [ConsultantTerminationTypeController::class, 'getAll'])->name('Get all consultant   Termination');
        Route::get('cons_terminate', [ConsultantTerminationTypeController::class, 'get'])->name('Get Consultant   Termination');
        Route::post('cons_terminate', [ConsultantTerminationTypeController::class, 'create'])->name('create Consultant   Termination');
        Route::put('cons_terminate', [ConsultantTerminationTypeController::class, 'update'])->name('update Consultant   Termination');
        Route::delete('cons_terminate', [ConsultantTerminationTypeController::class, 'delete'])->name('create Consultant   Termination');
    });
    Route::group(['tag' => 'Consultant Renewal Date'], function () {
        Route::get('cons_renewal', [ConsultantRenewalDateController::class, 'get'])->name('Get Consultant Renewal');
        Route::post('cons_renewal', [ConsultantRenewalDateController::class, 'create'])->name('create Consultant Renewal');
    });

    Route::group(['tag' => 'Distributor Application Approval'], function () {
        Route::get('distributor_new_application', [DistributorApprovalController::class, 'getDistributorApplicationList'])->name('Get distributor application list');
        // Route::get('distributor_review', [DistributorApprovalController::class, 'create'])->name('Update distributor application approval');
    });
    Route::get('document/{filename}', [DocumentController::class, 'getDocument']);
});
//Route::get('document/{filename}', [DocumentController::class, 'getDocument']);
Route::group(['tag' => 'api without middleware'], function () {

    Route::get('sidebarOE', [SideBarController::class, 'getSideBarByGroupId'])->name('Get sidebar by group Id');

    Route::get('default_country', [SettingGeneralController::class, 'getDefaultCountry'])->name('get default country');

    Route::post('send_email', [SettingEmailController::class, 'send'])->name('Send Email');
    Route::post('send_cas_email', [SettingEmailController::class, 'sendCasEmail'])->name('Send Cas Email Notification');
    Route::post('send_cas_barring', [SettingEmailController::class, 'sendCasBarring'])->name('Send Cas Email Barring Notification');
    Route::post('send_acceptance_email', [SettingEmailController::class, 'sendAcceptanceEmail'])->name('Send Acceptance Email Notification');
    Route::post('send_tp_email', [SettingEmailController::class, 'sendTPEmail'])->name('Send TP Email Notification');
    Route::post('send_acceptance_email', [SettingEmailController::class, 'sendAcceptanceEmail'])->name('Send Acceptance Email Notification');
    Route::post('send_dist_reg_email', [SettingEmailController::class, 'sendDistRegEmail'])->name('Send Distributor Registration Email Notification');
    Route::post('send_dist_reg_email_return', [SettingEmailController::class, 'sendDistRegEmailReturn'])->name('Send Distributor Registration Return Email Notification');
    Route::post('send_dist_reg_email_reject', [SettingEmailController::class, 'sendDistRegEmailReject'])->name('Send Distributor Registration Reject Email Notification');
    Route::post('send_suspendRevokeCeo_Email', [SettingEmailController::class, 'sendsuspendRevokeCeoEmail'])->name('Send suspend/Revoke Ceo Email Notification');
    Route::post('send_cease_email', [SettingEmailController::class, 'sendCeaseEmail'])->name('Send Cessation Email Notification');

    Route::post('get_sms_TAC', [SmsTacController::class, 'getTAC'])->name('Get TAC');
    Route::get('verify_TAC', [SmsTacController::class, 'verifyTAC'])->name('Verify TAC');
    Route::get('setting_password', [SettingPasswordController::class, 'get'])->name('Get password characteristic');
    Route::post('setting_password', [SettingPasswordController::class, 'create'])->name('Set password characteristic');
    Route::put('setting_password', [SettingPasswordController::class, 'update'])->name('Update password characteristic');
    Route::get('approval_level_byindexNM', [ApprovalLevelController::class, 'getByIndex'])->name('Get Approval Level by index');
});
Route::group(['tag' => 'FMS Fund Category '], function () {
    Route::get('fund_categorys', [FmsFundcategoryController::class, 'getAll'])->name('Get all Fund Category');
    Route::get('fund_category', [FmsFundcategoryController::class, 'get'])->name('Get Fund Category');
    Route::post('fund_category', [FmsFundcategoryController::class, 'create'])->name('create Fund Category');
    Route::put('fund_category', [FmsFundcategoryController::class, 'update'])->name('update Fund Category');
    Route::delete('fund_category', [FmsFundcategoryController::class, 'delete'])->name('create Fund Category');
});
Route::group(['tag' => 'FMS Fund Type '], function () {
    Route::get('fund_types', [FmsFundtypeController::class, 'getAll'])->name('Get all Fund Type ');
    Route::get('fund_type', [FmsFundtypeController::class, 'get'])->name('Get Fund Type ');
    Route::post('fund_type', [FmsFundtypeController::class, 'create'])->name('create Fund Type ');
    Route::put('fund_type', [FmsFundtypeController::class, 'update'])->name('update Fund Type ');
    Route::delete('fund_type', [FmsFundtypeController::class, 'delete'])->name('create Fund Type ');
});
Route::group(['tag' => 'FMS Fund Group '], function () {
    Route::get('fund_groups', [FmsFundgroupController::class, 'getAll'])->name('Get all Fund  Group ');
    Route::get('fund_group', [FmsFundgroupController::class, 'get'])->name('Get Fund  Group ');
    Route::post('fund_group', [FmsFundgroupController::class, 'create'])->name('create Fund  Group ');
    Route::put('fund_group', [FmsFundgroupController::class, 'update'])->name('update Fund  Group ');
    Route::delete('fund_group', [FmsFundgroupController::class, 'delete'])->name('create Fund  Group ');
});
Route::group(['tag' => 'FMS Fund Notes '], function () {
    Route::get('fund_notes', [FmsFundnotesController::class, 'getAll'])->name('Get all Fund Notes ');
    Route::get('fund_note', [FmsFundnotesController::class, 'get'])->name('Get Fund Notes ');
    Route::post('fund_note', [FmsFundnotesController::class, 'create'])->name('create Fund Notes ');
    Route::put('fund_note', [FmsFundnotesController::class, 'update'])->name('update Fund Notes ');
    Route::delete('fund_note', [FmsFundnotesController::class, 'delete'])->name('create Fund Notes ');
});
Route::group(['tag' => 'FMS Fund Domicile '], function () {
    Route::get('fund_domiciles', [FmsFundDomicileController::class, 'getAll'])->name('Get all Fund Domicile ');
    Route::get('fund_domicile', [FmsFundDomicileController::class, 'get'])->name('Get Fund Domicile ');
    Route::post('fund_domicile', [FmsFundDomicileController::class, 'create'])->name('create Fund Domicile ');
    Route::put('fund_domicile', [FmsFundDomicileController::class, 'update'])->name('update Fund Domicile ');
    Route::delete('fund_domicile', [FmsFundDomicileController::class, 'delete'])->name('create Fund Domicile ');
});
Route::group(['tag' => 'FMS Fund Scheme Structure '], function () {
    Route::get('fund_schemes', [FmsSchemeStructureController::class, 'getAll'])->name('Get all Fund Scheme Structure ');
    Route::get('fund_scheme', [FmsSchemeStructureController::class, 'get'])->name('Get Fund Scheme Structure ');
    Route::post('fund_scheme', [FmsSchemeStructureController::class, 'create'])->name('create Fund Scheme Structure ');
    Route::put('fund_scheme', [FmsSchemeStructureController::class, 'update'])->name('update Fund Scheme Structure ');
    Route::delete('fund_scheme', [FmsSchemeStructureController::class, 'delete'])->name('create Fund Scheme Structure ');
});
Route::group(['tag' => 'FMS Fund Remark option '], function () {
    Route::get('fund_remarks', [FmsRemarkOptionController::class, 'getAll'])->name('Get all Fund Remark option ');
    Route::get('fund_remark', [FmsRemarkOptionController::class, 'get'])->name('Get Fund Remark option ');
    Route::post('fund_remark', [FmsRemarkOptionController::class, 'create'])->name('create Fund Remark option ');
    Route::put('fund_remark', [FmsRemarkOptionController::class, 'update'])->name('update Fund Remark option ');
    Route::delete('fund_remark', [FmsRemarkOptionController::class, 'delete'])->name('create Fund Remark option ');
});
Route::group(['tag' => 'FMS Fund Reason option '], function () {
    Route::get('fund_reasons', [FmsReasonOptionController::class, 'getAll'])->name('Get all Fund  Reason option ');
    Route::get('fund_reason', [FmsReasonOptionController::class, 'get'])->name('Get Fund  Reason option ');
    Route::post('fund_reason', [FmsReasonOptionController::class, 'create'])->name('create Fund  Reason option ');
    Route::put('fund_reason', [FmsReasonOptionController::class, 'update'])->name('update Fund  Reason option ');
    Route::delete('fund_reason', [FmsReasonOptionController::class, 'delete'])->name('create Fund  Reason option ');
});
Route::group(['tag' => 'Fund Cut Off time'], function () {
    Route::get('fund_cuttime', [FmsCutoffTimeController::class, 'get'])->name('Get Fund Cut Off time');
    Route::post('fund_cuttime', [FmsCutoffTimeController::class, 'create'])->name('create Fund Cut Off time ');
});
Route::group(['tag' => 'Distributor Fee'], function () {
    Route::get('dist_fee_type', [DistributorFeeController::class, 'getDistributor'])->name('Get Dist Fee');
    Route::get('dist_fees', [DistributorFeeController::class, 'getAll'])->name('Get all Dist Fee ');
    Route::post('dist_fee', [DistributorFeeController::class, 'create'])->name('create Dist Fee ');
    Route::get('dist_fee', [DistributorFeeController::class, 'get'])->name('Get Dist Fee');
    Route::put('dist_fee', [DistributorFeeController::class, 'update'])->name('update Dist Fee ');
    Route::delete('dist_fee', [DistributorFeeController::class, 'delete'])->name('create Dist Fee ');

    Route::get('getDistFeeByIDType', [DistributorFeeController::class, 'getDistFeeByIDType'])->name('Get all Dist Fee by id type ');
});
Route::group(['tag' => 'Waiver Fee'], function () {
    Route::get('waiver_cons_type', [WaiverFeeController::class, 'getConsType'])->name('Get Waiver Fee');
    Route::get('waiver_exam_type', [WaiverFeeController::class, 'getExamType'])->name('Get Waiver Fee');
    Route::get('waiver_type', [WaiverFeeController::class, 'getWaiverType'])->name('Get Waiver Fee');
    Route::get('waiver_fees', [WaiverFeeController::class, 'getAll'])->name('Get all Waiver Fee ');
    Route::post('waiver_fee', [WaiverFeeController::class, 'create'])->name('create Waiver Fee ');
    Route::get('waiver_fee', [WaiverFeeController::class, 'get'])->name('Get Waiver Fee');
    Route::put('waiver_fee', [WaiverFeeController::class, 'update'])->name('update Waiver Fee ');
    Route::delete('waiver_fee', [WaiverFeeController::class, 'delete'])->name('create Waiver Fee ');
    Route::get('fee_type', [WaiverFeeController::class, 'getFeeType'])->name('Get Waiver Fee');
});

Route::group(['tag' => 'Consultant Fee'], function () {
    // Route::get('cons_type_fee', [ConsultantFeeController::class, 'getAllConsType'])->name('Get Consultant Fee');
    // Route::get('cons_exam_type', [ConsultantFeeController::class, 'getAllExamType'])->name('Get Consultant Fee');
    Route::get('cons_fees', [ConsultantFeeController::class, 'getAll'])->name('Get all Consultant Fee ');
    Route::post('cons_fee', [ConsultantFeeController::class, 'create'])->name('create Consultant Fee ');
    Route::get('cons_fee', [ConsultantFeeController::class, 'get'])->name('Get Consultant Fee');
    Route::put('cons_fee', [ConsultantFeeController::class, 'update'])->name('update Consultant Fee ');
    Route::delete('cons_fee', [ConsultantFeeController::class, 'delete'])->name('create Consultant Fee ');
});
Route::group(['tag' => 'Annual Fee Invoice'], function () {
    Route::get('annual_invoices', [AnnualFeeInvoiceController::class, 'getAll'])->name('Get all Annual Fee Invoice ');
    Route::post('annual_invoice', [AnnualFeeInvoiceController::class, 'create'])->name('create Annual Fee Invoice ');
    Route::get('annual_invoice', [AnnualFeeInvoiceController::class, 'get'])->name('Get Annual Fee Invoice');
    Route::put('annual_invoice', [AnnualFeeInvoiceController::class, 'update'])->name('update Annual Fee Invoice ');
    Route::delete('annual_invoice', [AnnualFeeInvoiceController::class, 'delete'])->name('create Annual Fee Invoice ');
});
Route::group(['tag' => 'FMS Umbrella Fund'], function () {
    Route::get('umbrella_funds', [FmsUmbrellaFundController::class, 'getAll'])->name('Get all FMS Umbrella Fund ');
    Route::post('umbrella_fund', [FmsUmbrellaFundController::class, 'create'])->name('create FMS Umbrella Fund ');
    Route::get('umbrella_fund', [FmsUmbrellaFundController::class, 'get'])->name('Get FMS Umbrella Fund');
    Route::put('umbrella_fund', [FmsUmbrellaFundController::class, 'update'])->name('update FMS Umbrella Fund ');
    Route::delete('umbrella_fund', [FmsUmbrellaFundController::class, 'delete'])->name('create FMS Umbrella Fund ');
});
Route::group(['tag' => 'Login Setting'], function () {
    Route::get('login_setting', [LoginSettingController::class, 'get'])->name('Get Login');
    Route::post('login_setting', [LoginSettingController::class, 'create'])->name('create login ');
});
Route::group(['tag' => 'Login Duration'], function () {
    Route::get('login_duration', [SystemBlockDurationController::class, 'get'])->name('Get Login');
    Route::post('login_duration', [SystemBlockDurationController::class, 'create'])->name('create login ');
});
Route::group(['tag' => 'Login Session'], function () {
    Route::get('login_session', [LoginIdleSessionController::class, 'get'])->name('Get Login');
    Route::post('login_session', [LoginIdleSessionController::class, 'create'])->name('create login ');
});
Route::group(['tag' => 'Password History'], function () {
    Route::get('password_history', [PasswordHistoryController::class, 'get'])->name('Get Login');
    Route::post('password_history', [PasswordHistoryController::class, 'create'])->name('create login ');
});
Route::group(['tag' => 'Password Default'], function () {
    Route::get('password_default', [PasswordDefaultController::class, 'get'])->name('Get Password Default');
    Route::post('password_default', [PasswordDefaultController::class, 'create'])->name('create Password Default ');
});
Route::group(['tag' => 'User ID'], function () {
    Route::get('user_id', [SettingUseridController::class, 'get'])->name('Get user ID');
    Route::post('user_id', [SettingUseridController::class, 'create'])->name('create User ID ');
});
Route::group(['tag' => 'Dist Manage Group '], function () {
    Route::get('dist_manage_groups', [DistributorManageGroupController::class, 'getAll'])->name('Get all Dist Manage Group  ');
    Route::get('dist_manage_group', [DistributorManageGroupController::class, 'get'])->name('Get Dist Manage Group  ');
    Route::post('dist_manage_group', [DistributorManageGroupController::class, 'create'])->name('create Dist Manage Group  ');
    Route::put('dist_manage_group', [DistributorManageGroupController::class, 'update'])->name('update Dist Manage Group  ');
    Route::delete('dist_manage_group', [DistributorManageGroupController::class, 'delete'])->name('create Dist Manage Group  ');
});
Route::group(['tag' => 'Dist Manage Module '], function () {
    Route::get('dist_manage_modules', [DistributorManageModuleController::class, 'getAll'])->name('Get all Dist Manage Modules');
    Route::get('dist_manage_module', [DistributorManageModuleController::class, 'get'])->name('Get Dist Manage Modules');
    Route::post('dist_manage_module', [DistributorManageModuleController::class, 'create'])->name('create Dist Manage Modules');
    Route::put('dist_manage_module', [DistributorManageModuleController::class, 'update'])->name('update Dist Manage Modules');
    Route::delete('dist_manage_module', [DistributorManageModuleController::class, 'delete'])->name('create Dist Manage Modules');
});
Route::group(['tag' => 'Dist Manage SubModule '], function () {
    Route::get('dist_manage_submodules', [DistributorManageSubmoduleController::class, 'getAll'])->name('Get all Dist Manage SubModule');
    Route::get('dist_manage_submodule', [DistributorManageSubmoduleController::class, 'get'])->name('Get Dist Manage SubModule');
    Route::get('dist_get_module', [DistributorManageSubmoduleController::class, 'getModule'])->name('Get Dist Get Module');
    Route::post('dist_manage_submodule', [DistributorManageSubmoduleController::class, 'create'])->name('create Dist Manage SubModule');
    Route::put('dist_manage_submodule', [DistributorManageSubmoduleController::class, 'update'])->name('update Dist Manage SubModule');
    Route::delete('dist_manage_submodule', [DistributorManageSubmoduleController::class, 'delete'])->name('create Dist Manage SubModule');
});
Route::group(['tag' => 'Dist Manage Screen '], function () {
    Route::get('dist_manage_screens', [DistributorManageScreenController::class, 'getAll'])->name('Get all Dist Manage SubModule');
    Route::get('dist_manage_screen', [DistributorManageScreenController::class, 'get'])->name('Get Dist Manage SubModule');
    Route::get('dist_get_module', [DistributorManageScreenController::class, 'getScreen'])->name('Get Dist Get Module');
    Route::get('dist_get_processflow', [DistributorManageScreenController::class, 'getProcessFlow'])->name('Get Dist Manage SubModule');
    Route::get('dist_manage_screen', [DistributorManageScreenController::class, 'getSubmodule'])->name('Get Dist Manage SubModule');
    Route::post('dist_manage_screen', [DistributorManageScreenController::class, 'create'])->name('create Dist Manage SubModule');
    Route::put('dist_manage_screen', [DistributorManageScreenController::class, 'update'])->name('update Dist Manage SubModule');
    Route::delete('dist_manage_screen', [DistributorManageScreenController::class, 'delete'])->name('create Dist Manage SubModule');
});
Route::group(['tag' => 'CAS letter'], function () {
    Route::get('letter_list', [CasLetterController::class, 'getAll'])->name('Get all letter list ');
    Route::post('letter_create', [CasLetterController::class, 'create'])->name('create letter template');
    Route::delete('delete_Letter', [CasLetterController::class, 'delete'])->name('create letter ');
    Route::get('edit_Letter', [CasLetterController::class, 'get'])->name('Get letter by Id');
    Route::put('update_letter', [CasLetterController::class, 'update'])->name('update letter');
});

Route::group(['tag' => 'Distributor Screen Access'], function () {
    Route::get('dist_screen_access', [DistributorScreenAccessController::class, 'get'])->name('Get screen access by Id');
    Route::get('dist_screen_access_user', [DistributorScreenAccessController::class, 'getUser'])->name('Get user');
    Route::get('dist_screen_access_auth', [DistributorScreenAccessController::class, 'getAuthLevel'])->name('Get Auth Level');
    Route::get('dist_screen_access_authpage', [DistributorScreenAccessController::class, 'getAuthorisationPage'])->name('Get Auth Level');
    Route::get('dist_screen_accesses', [DistributorScreenAccessController::class, 'getAll'])->name('Get all screen access');
    Route::post('dist_screen_access', [DistributorScreenAccessController::class, 'create'])->name('Create screen access');
    Route::put('dist_screen_access', [DistributorScreenAccessController::class, 'update'])->name('update Dist Manage screen Access');
});
Route::group(['tag' => 'Cons Manage Group '], function () {
    Route::get('cons_manage_groups', [ConsultantManageGroupController::class, 'getAll'])->name('Get all cons Manage Group  ');
    Route::get('cons_manage_group', [ConsultantManageGroupController::class, 'get'])->name('Get cons Manage Group  ');
    Route::post('cons_manage_group', [ConsultantManageGroupController::class, 'create'])->name('create cons Manage Group  ');
    Route::put('cons_manage_group', [ConsultantManageGroupController::class, 'update'])->name('update cons Manage Group  ');
    Route::delete('cons_manage_group', [ConsultantManageGroupController::class, 'delete'])->name('create cons Manage Group  ');
});
Route::group(['tag' => 'Cons Manage Module '], function () {
    Route::get('cons_manage_modules', [ConsultantManageModuleController::class, 'getAll'])->name('Get all cons Manage Modules');
    Route::get('cons_manage_module', [ConsultantManageModuleController::class, 'get'])->name('Get cons Manage Modules');
    Route::post('cons_manage_module', [ConsultantManageModuleController::class, 'create'])->name('create cons Manage Modules');
    Route::put('cons_manage_module', [ConsultantManageModuleController::class, 'update'])->name('update cons Manage Modules');
    Route::delete('cons_manage_module', [ConsultantManageModuleController::class, 'delete'])->name('create cons Manage Modules');
});
Route::group(['tag' => 'Cons Manage SubModule '], function () {
    Route::get('cons_manage_submodules', [ConsultantManageSubmoduleController::class, 'getAll'])->name('Get all cons Manage SubModule');
    Route::get('cons_manage_submodule', [ConsultantManageSubmoduleController::class, 'get'])->name('Get cons Manage SubModule');
    Route::get('cons_get_module', [ConsultantManageSubmoduleController::class, 'getConsModule'])->name('Get cons Get Module');
    Route::post('cons_manage_submodule', [ConsultantManageSubmoduleController::class, 'create'])->name('create cons Manage SubModule');
    Route::put('cons_manage_submodule', [ConsultantManageSubmoduleController::class, 'update'])->name('update cons Manage SubModule');
    Route::delete('cons_manage_submodule', [ConsultantManageSubmoduleController::class, 'delete'])->name('create cons Manage SubModule');
});
Route::group(['tag' => 'cons Manage Screen '], function () {
    Route::get('cons_manage_screens', [ConsultantManageScreenController::class, 'getAll'])->name('Get all cons Manage SubModule');
    Route::get('cons_manage_screen', [ConsultantManageScreenController::class, 'get'])->name('Get cons Manage SubModule');
    //Route::get('cons_get_module', [ConsultantManageScreenController::class, 'getScreen'])->name('Get cons Get Module');
    Route::get('cons_get_processflow', [ConsultantManageScreenController::class, 'getProcessFlow'])->name('Get cons Manage SubModule');
    Route::get('cons_get_submodule', [ConsultantManageScreenController::class, 'getSubmodule'])->name('Get cons Manage SubModule');
    Route::post('cons_manage_screen', [ConsultantManageScreenController::class, 'create'])->name('create cons Manage SubModule');
    Route::put('cons_manage_screen', [ConsultantManageScreenController::class, 'update'])->name('update cons Manage SubModule');
    Route::delete('cons_manage_screen', [ConsultantManageScreenController::class, 'delete'])->name('create cons Manage SubModule');
});
Route::group(['tag' => 'Cons Screen Access'], function () {
    Route::get('cons_screen_access', [ConsultantScreenAccessController::class, 'get'])->name('Get screen access by Id');
    Route::get('cons_screen_access_user', [ConsultantScreenAccessController::class, 'getUser'])->name('Get user');
    Route::get('cons_screen_access_auth', [ConsultantScreenAccessController::class, 'getAuthLevel'])->name('Get Auth Level');
    Route::get('cons_screen_access_authpage', [ConsultantScreenAccessController::class, 'getAuthorisationPage'])->name('Get Auth Level');
    Route::get('cons_screen_accesses', [ConsultantScreenAccessController::class, 'getAll'])->name('Get all screen access');
    Route::post('cons_screen_access', [ConsultantScreenAccessController::class, 'create'])->name('Create screen access');
    Route::put('cons_screen_access', [ConsultantScreenAccessController::class, 'update'])->name('update Dist Manage screen Access');
});
Route::group(['tag' => 'Third Party Manage Group '], function () {
    Route::get('third_manage_groups', [ThirdpartyManageGroupController::class, 'getAll'])->name('Get all third Manage Group  ');
    Route::get('third_manage_group', [ThirdpartyManageGroupController::class, 'get'])->name('Get third Manage Group  ');
    Route::post('third_manage_group', [ThirdpartyManageGroupController::class, 'create'])->name('create third Manage Group  ');
    Route::put('third_manage_group', [ThirdpartyManageGroupController::class, 'update'])->name('update third Manage Group  ');
    Route::delete('third_manage_group', [ThirdpartyManageGroupController::class, 'delete'])->name('create third Manage Group  ');
});
Route::group(['tag' => 'third Manage Module '], function () {
    Route::get('third_manage_modules', [ThirdpartyManageModuleController::class, 'getAll'])->name('Get all third Manage Modules');
    Route::get('third_manage_module', [ThirdpartyManageModuleController::class, 'get'])->name('Get third Manage Modules');
    Route::post('third_manage_module', [ThirdpartyManageModuleController::class, 'create'])->name('create third Manage Modules');
    Route::put('third_manage_module', [ThirdpartyManageModuleController::class, 'update'])->name('update third Manage Modules');
    Route::delete('third_manage_module', [ThirdpartyManageModuleController::class, 'delete'])->name('create third Manage Modules');
});
Route::group(['tag' => 'Third Party Manage SubModule '], function () {
    Route::get('third_manage_submodules', [ThirdpartyManageSubmoduleController::class, 'getAll'])->name('Get all third Manage SubModule');
    Route::get('third_manage_submodule', [ThirdpartyManageSubmoduleController::class, 'get'])->name('Get third Manage SubModule');
    Route::get('third_get_module', [ThirdpartyManageSubmoduleController::class, 'getThirdModule'])->name('Get third Get Module');
    Route::post('third_manage_submodule', [ThirdpartyManageSubmoduleController::class, 'create'])->name('create third Manage SubModule');
    Route::put('third_manage_submodule', [ThirdpartyManageSubmoduleController::class, 'update'])->name('update third Manage SubModule');
    Route::delete('third_manage_submodule', [ThirdpartyManageSubmoduleController::class, 'delete'])->name('create third Manage SubModule');
});
Route::group(['tag' => 'Third Party Manage Screen '], function () {
    Route::get('third_manage_screens', [ThirdPartyManageScreenController::class, 'getAll'])->name('Get all third Manage SubModule');
    Route::get('third_manage_screen', [ThirdPartyManageScreenController::class, 'get'])->name('Get third Manage SubModule');
    //Route::get('third_get_module', [ThirdPartyManageScreenController::class, 'getScreen'])->name('Get third Get Module');
    Route::get('third_get_processflow', [ThirdPartyManageScreenController::class, 'getProcessFlow'])->name('Get third Manage SubModule');
    Route::get('third_get_submodule', [ThirdPartyManageScreenController::class, 'getSubmodule'])->name('Get third Manage SubModule');
    Route::post('third_manage_screen', [ThirdPartyManageScreenController::class, 'create'])->name('create third Manage SubModule');
    Route::put('third_manage_screen', [ThirdPartyManageScreenController::class, 'update'])->name('update third Manage SubModule');
    Route::delete('third_manage_screen', [ThirdPartyManageScreenController::class, 'delete'])->name('create third Manage SubModule');
});
Route::group(['tag' => 'Third Party Screen Access'], function () {
    Route::get('third_screen_access', [ThirdpartyScreenAccessController::class, 'get'])->name('Get screen access by Id');
    Route::get('third_screen_access_user', [ThirdpartyScreenAccessController::class, 'getUser'])->name('Get user');
    Route::get('third_screen_access_auth', [ThirdpartyScreenAccessController::class, 'getAuthLevel'])->name('Get Auth Level');
    Route::get('third_screen_access_authpage', [ThirdpartyScreenAccessController::class, 'getAuthorisationPage'])->name('Get Auth Level');
    Route::get('third_screen_accesses', [ThirdpartyScreenAccessController::class, 'getAll'])->name('Get all screen access');
    Route::post('third_screen_access', [ThirdpartyScreenAccessController::class, 'create'])->name('Create screen access');
    Route::put('third_screen_access', [ThirdpartyScreenAccessController::class, 'update'])->name('update Dist Manage screen Access');
});
Route::group(['tag' => 'Training Provider Manage Group '], function () {
    Route::get('tp_manage_groups', [TpManageGroupController::class, 'getAll'])->name('Get all tp Manage Group  ');
    Route::get('tp_manage_group', [TpManageGroupController::class, 'get'])->name('Get tp Manage Group  ');
    Route::post('tp_manage_group', [TpManageGroupController::class, 'create'])->name('create tp Manage Group  ');
    Route::put('tp_manage_group', [TpManageGroupController::class, 'update'])->name('update tp Manage Group  ');
    Route::delete('tp_manage_group', [TpManageGroupController::class, 'delete'])->name('create tp Manage Group  ');
});
Route::group(['tag' => 'Training Provider Manage Module '], function () {
    Route::get('tp_manage_modules', [TpManageModuleController::class, 'getAll'])->name('Get all tp Manage Modules');
    Route::get('tp_manage_module', [TpManageModuleController::class, 'get'])->name('Get tp Manage Modules');
    Route::post('tp_manage_module', [TpManageModuleController::class, 'create'])->name('create tp Manage Modules');
    Route::put('tp_manage_module', [TpManageModuleController::class, 'update'])->name('update tp Manage Modules');
    Route::delete('tp_manage_module', [TpManageModuleController::class, 'delete'])->name('create tp Manage Modules');
});
Route::group(['tag' => 'Training Provider Manage SubModule '], function () {
    Route::get('tp_manage_submodules', [TpManageSubmoduleController::class, 'getAll'])->name('Get all tp Manage SubModule');
    Route::get('tp_manage_submodule', [TpManageSubmoduleController::class, 'get'])->name('Get tp Manage SubModule');
    Route::get('tp_get_module', [TpManageSubmoduleController::class, 'getThirdModule'])->name('Get tp Get Module');
    Route::post('tp_manage_submodule', [TpManageSubmoduleController::class, 'create'])->name('create tp Manage SubModule');
    Route::put('tp_manage_submodule', [TpManageSubmoduleController::class, 'update'])->name('update tp Manage SubModule');
    Route::delete('tp_manage_submodule', [TpManageSubmoduleController::class, 'delete'])->name('create tp Manage SubModule');
});
Route::group(['tag' => 'Training Provider Manage Screen '], function () {
    Route::get('tp_manage_screens', [TpManageScreenController::class, 'getAll'])->name('Get all tp Manage SubModule');
    Route::get('tp_manage_screen', [TpManageScreenController::class, 'get'])->name('Get tp Manage SubModule');
    // Route::get('tp_get_module', [TpManageScreenController::class, 'getScreen'])->name('Get tp Get Module');
    Route::get('tp_get_processflow', [TpManageScreenController::class, 'getProcessFlow'])->name('Get tp Manage SubModule');
    Route::get('tp_get_submodule', [TpManageScreenController::class, 'getSubmodule'])->name('Get tp Manage SubModule');
    Route::post('tp_manage_screen', [TpManageScreenController::class, 'create'])->name('create tp Manage SubModule');
    Route::put('tp_manage_screen', [TpManageScreenController::class, 'update'])->name('update tp Manage SubModule');
    Route::delete('tp_manage_screen', [TpManageScreenController::class, 'delete'])->name('create tp Manage SubModule');
});
Route::group(['tag' => 'Training Provider Screen Access'], function () {
    Route::get('tp_screen_access', [TpScreenAccessController::class, 'get'])->name('Get screen access by Id');
    Route::get('tp_screen_access_user', [TpScreenAccessController::class, 'getUser'])->name('Get user');
    Route::get('tp_screen_access_auth', [TpScreenAccessController::class, 'getAuthLevel'])->name('Get Auth Level');
    Route::get('tp_screen_access_authpage', [TpScreenAccessController::class, 'getAuthorisationPage'])->name('Get Auth Level');
    Route::get('tp_screen_accesses', [TpScreenAccessController::class, 'getAll'])->name('Get all screen access');
    Route::post('tp_screen_access', [TpScreenAccessController::class, 'create'])->name('Create screen access');
    Route::put('tp_screen_access', [TpScreenAccessController::class, 'update'])->name('update Dist Manage screen Access');
});
Route::group(['tag' => 'Distributor Approval Level'], function () {
    Route::get('dist_approval_level_byindex', [DistributorApprovalLevelController::class, 'getDistByIndex'])->name('Get DIstributor Approval Level by index');
});
Route::group(['tag' => 'Page Maintenance'], function () {
    Route::get('page_list', [PageMaintenanceController::class, 'getAll'])->name('Get all letter list ');
    Route::post('page_create', [PageMaintenanceController::class, 'create'])->name('create letter template');
    Route::delete('delete_page', [PageMaintenanceController::class, 'delete'])->name('create letter ');
    Route::get('edit_page', [PageMaintenanceController::class, 'get'])->name('Get letter by Id');
    Route::put('update_page', [PageMaintenanceController::class, 'update'])->name('update page maintanance');
    Route::get('get_audience', [PageMaintenanceController::class, 'getAudience'])->name('Get Audience');
});
